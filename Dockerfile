FROM ruby:2.4-alpine
MAINTAINER edjr.gonzales@gmail.com

RUN apk update && apk upgrade && \
	apk add ruby ruby-json ruby-io-console ruby-bundler ruby-irb ruby-bigdecimal tzdata && \
	apk add nodejs && \
	apk add curl-dev ruby-dev build-base libffi-dev && \
	apk add build-base libxslt-dev libxml2-dev ruby-rdoc mysql-dev sqlite-dev

RUN gem install bundler execjs

RUN mkdir -p /marcat-app
WORKDIR /marcat-app

COPY Gemfile Gemfile.lock ./
RUN bundle install --binstubs --jobs 20 --retry 5

COPY . .

EXPOSE 3000
