FactoryBot.define do
  factory :warehouse do
    name {'Warehouse 1'}
    wh_code {'TSTWRHSE'}
    wh_address {'21 Lucky St., Brgy. Parang, Quezon City'}
    status { Warehouse.statuses[:active] }

    factory :warehouse_with_section do
      name {'Warehouse With 1 Section'}
      wh_code {'TSTWRHSE_WSCT1'}

      transient do
        section_count { 1 }
      end

      after(:create) do |warehouse, evaluator|
        create_list(:stock_section, evaluator.section_count, warehouse: warehouse)
      end
    end
  end
end
