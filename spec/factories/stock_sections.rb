FactoryBot.define do
  factory :stock_section do
    warehouse { Warehouse.first.present? ? Warehouse.first : create(:warehouse) }

    name {'Supply Section, Room 2'}
    sc_code {'TSTWRHSE-SCT1'}
  end
end
