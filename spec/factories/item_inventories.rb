FactoryBot.define do
  factory :item_inventory do
    user { User.first.present? ? User.first : create(:user) }
    stock_section { StockSection.first.present? ? StockSection.first : create(:stock_section) }
    item

    item_status { ItemInventory.item_statuses[:stock] }
    serial_id { '' }

    factory :with_serial_id1 do
      serial_id { 'SRLNO1' }
    end

    factory :with_serial_id2 do
      serial_id { 'SRLNO2' }
    end
  end
end
