FactoryBot.define do
  factory :item do
    name {'Product 1'}
    details {'Simple Test Product for RSpec'}
    price {2}
    dealer_price {1}
    srp_price {3}
    sku {'SKU0'}
    status {1}

    factory :simple_item_with_5_item_inventory do
      name {'Simple Product 1'}
      sku { 'SKU1' }

      transient do
        inv_count { 5 }
      end

      after(:create) do |item, evaluator|
        create_list(:item_inventory, evaluator.inv_count, item: item )
      end
    end

    factory :identifiable_item_with_2_item_inventory do
      name {'Identifiable Product 1'}
      sku { 'SKUID1' }
      is_identifiable { true }

      after(:create) do |item, evaluator|
        item.item_inventories << create(:with_serial_id1, item: item)
        item.item_inventories << create(:with_serial_id2, item: item)
      end
    end
  end
end
