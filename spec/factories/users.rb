FactoryBot.define do
  factory :user do
    username {'test_user'}
    email {'user@test.com'}
    password {'P@55w0rd'}
    status { User.statuses[:active] }
  end
end
