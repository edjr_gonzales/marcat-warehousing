require "rails_helper"

RSpec.describe Item, :type => :model do
  before(:all) do
    @simple_product = create(:simple_item_with_5_item_inventory)
    @identifiable_product = create(:identifiable_item_with_2_item_inventory)
  end

  it "saving non identifiable produces no errors" do
    expect(@simple_product.errors.size).to eq(0)
  end

  it "saving identifiable produces no errors" do
    expect(@identifiable_product.errors.size).to eq(0)
  end

  it "produces 2 samples" do
    items = Item.all
    expect(items.size).to eq(2)
  end

  it "produces 1 non identifiable" do
    non_identifiable = Item.where(is_identifiable: false)
    expect(non_identifiable.size).to eq(1)
  end

  it "non identifiable contains 5 inventories" do
    expect(@simple_product.item_inventories.size).to eq(5)
  end

  it "produces 1 identifiable" do
    identifiable = Item.where(is_identifiable: true)
    expect(identifiable.size).to eq(1)
  end

  it "identifiable contains 2 inventories" do
    expect(@identifiable_product.item_inventories.size).to eq(2)
  end
end
