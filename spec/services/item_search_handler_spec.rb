require "rails_helper"
require 'faker'

RSpec.describe ItemSearchHandler, :type => :service do
  before(:all) do
    @common_keyword = "RSPEC KEYWORD"
    @simple_product = create(:simple_item_with_5_item_inventory, name: "#{@common_keyword} #{Faker::Book.title}")
    @identifiable_product = create(:identifiable_item_with_2_item_inventory, name: "#{@common_keyword} #{Faker::Vehicle.make_and_model}")
  end

  context "#search_non_identifiables" do
    it "yields correct count of non identifiable inventory" do
      handler = ItemSearchHandler.new(@simple_product.name)
      items = handler.search_non_identifiables
      expect(items.first.stock_quantity).to eq(@simple_product.item_inventories.size)
    end

    it "yields correct count of non identifiable inventory using blank keyword" do
      handler = ItemSearchHandler.new
      items = handler.search_non_identifiables
      expect(items.first.stock_quantity).to eq(@simple_product.item_inventories.size)
    end
  end

  context "#search_identifiables" do
    it "yields correct count of identifiable inventory" do
      handler = ItemSearchHandler.new(@identifiable_product.name)
      items = handler.search_identifiables
      expect(items.size).to eq(@identifiable_product.item_inventories.size)
    end

    it "yields correct count of identifiable inventory using blank keyword" do
      handler = ItemSearchHandler.new
      items = handler.search_identifiables
      expect(items.to_a.size).to eq(@identifiable_product.item_inventories.size)
    end
  end

  context "#execute" do
    it "retrieves all available non-identifiable inventory by search" do
      service = ItemSearchHandler.execute(@simple_product.name)
      expect(service.results.first.stock_quantity).to eq(@simple_product.item_inventories.size)
    end

    it "retrieves all available non-identifiable inventory by search using blank keyword" do
      service = ItemSearchHandler.execute
      expect(service.results.first.stock_quantity).to eq(@simple_product.item_inventories.size)
    end

    it "retrieves all available identifiable inventory by search" do
      service = ItemSearchHandler.execute(@identifiable_product.name)
      expect(service.results.size).to eq(@identifiable_product.item_inventories.size)
    end

    it "retrieves all available identifiable inventory by search using blank keyword" do
      service = ItemSearchHandler.execute
      expect(service.results.size).to eq(@identifiable_product.item_inventories.size + 1)
    end

    it "retrieves all available identifiable and non-identifiable inventory by search" do
      service = ItemSearchHandler.execute(@common_keyword)

      # first few items of results contains non-identifiable stocks
      expect(service.results.first.stock_quantity).to eq(@simple_product.item_inventories.size)

      # the ending set of results contains identifiable stocks and their stock_quantity is always 1
      expect(service.results.last.stock_quantity).to eq(1)

      # total result my contain all non-identifiable and identifiable
      expect(service.results.size).to eq(@identifiable_product.item_inventories.size + 1)
    end

    it "retrieves all available identifiable and non-identifiable inventory by search using blank keyword" do
      service = ItemSearchHandler.execute

      # first few items of results contains non-identifiable stocks
      expect(service.results.first.stock_quantity).to eq(@simple_product.item_inventories.size)

      # the ending set of results contains identifiable stocks and their stock_quantity is always 1
      expect(service.results.last.stock_quantity).to eq(1)

      # total result my contain all non-identifiable and identifiable
      expect(service.results.size).to eq(@identifiable_product.item_inventories.size + 1)
    end
  end
end
