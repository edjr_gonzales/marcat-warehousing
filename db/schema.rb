# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_15_152226) do

  create_table "accounting_informations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "supplier_id"
    t.integer "credit_term"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["supplier_id"], name: "index_accounting_informations_on_supplier_id"
  end

  create_table "active_storage_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "activity_logs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "user_accountable_type"
    t.bigint "user_accountable_id"
    t.bigint "user_id"
    t.string "log_type"
    t.string "log_message"
    t.integer "updated_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_accountable_type", "user_accountable_id"], name: "activity_logs_index"
    t.index ["user_id"], name: "index_activity_logs_on_user_id"
  end

  create_table "addresses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "customer_address_id"
    t.string "apt_bldg_suite"
    t.string "unit"
    t.string "floor"
    t.string "lot_no"
    t.string "street"
    t.string "brgy"
    t.string "district"
    t.string "town_city"
    t.string "postal_code"
    t.string "state_province"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "addressable_id"
    t.string "addressable_type"
    t.string "basic_address"
    t.index ["customer_address_id"], name: "index_addresses_on_customer_address_id"
  end

  create_table "branch_assignments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "branchable_type"
    t.bigint "branchable_id"
    t.bigint "contact_information_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["branchable_type", "branchable_id"], name: "contactable_index"
    t.index ["contact_information_id"], name: "index_branch_assignments_on_contact_information_id"
  end

  create_table "brands", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "short_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bundled_items", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "item_id"
    t.bigint "item_bundle_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_bundle_id"], name: "index_bundled_items_on_item_bundle_id"
    t.index ["item_id"], name: "index_bundled_items_on_item_id"
  end

  create_table "categories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "short_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "parent_id"
    t.integer "status"
  end

  create_table "collection_informations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "customer_id"
    t.integer "credit_term"
    t.decimal "credit_limit", precision: 15, scale: 4
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "collection_day"
    t.string "collection_time"
    t.string "counter_day"
    t.string "counter_time"
    t.index ["customer_id"], name: "index_collection_informations_on_customer_id"
  end

  create_table "contact_informations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "contact_number_1"
    t.string "contact_number_2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "contactable_id"
    t.string "contactable_type"
    t.string "email"
  end

  create_table "countries", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_addresses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "customer_id"
    t.boolean "is_primary"
    t.boolean "is_business_address"
    t.boolean "is_home_address"
    t.string "fax"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_delivery_address"
    t.string "delivery_schedule_day"
    t.string "delivery_schedule_time"
    t.string "business_name"
    t.string "tax_id"
    t.index ["customer_id"], name: "index_customer_addresses_on_customer_id"
  end

  create_table "customer_discounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "discount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "customer_id"
    t.integer "brand_id"
    t.integer "from_month"
    t.integer "to_month"
  end

  create_table "customer_order_checkers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "customer_order_id"
    t.bigint "user_id"
    t.index ["customer_order_id"], name: "index_customer_order_checkers_on_customer_order_id"
    t.index ["user_id"], name: "index_customer_order_checkers_on_user_id"
  end

  create_table "customer_order_items", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "customer_order_id"
    t.bigint "item_id"
    t.float "total_discount", default: 0.0
    t.json "sales_data"
    t.integer "net_amount_cents", default: 0, null: false
    t.string "net_amount_currency", default: "PHP", null: false
    t.integer "gross_amount_cents", default: 0, null: false
    t.string "gross_amount_currency", default: "PHP", null: false
    t.index ["customer_order_id"], name: "index_customer_order_items_on_customer_order_id"
    t.index ["item_id"], name: "index_customer_order_items_on_item_id"
  end

  create_table "customer_order_statuses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "customer_orders_id"
    t.integer "order_status"
    t.string "status_detail", limit: 512
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_orders_id"], name: "index_customer_order_statuses_on_customer_orders_id"
  end

  create_table "customer_orders", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "customer_id"
    t.bigint "warehouse_id"
    t.bigint "sales_person_id"
    t.integer "order_slip_id", default: 0
    t.integer "shipping_address_id"
    t.integer "billing_address_id"
    t.string "trans_ref_number"
    t.integer "gross_sales_cents", default: 0, null: false
    t.string "gross_sales_currency", default: "PHP", null: false
    t.integer "net_sales_cents", default: 0, null: false
    t.string "net_sales_currency", default: "PHP", null: false
    t.integer "net_amount_due_cents", default: 0, null: false
    t.string "net_amount_due_currency", default: "PHP", null: false
    t.date "delivery_date"
    t.json "doc_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "sale_date"
    t.boolean "saved", default: false
    t.string "po_reference", default: ""
    t.boolean "removed", default: false
    t.datetime "removed_at"
    t.integer "salesman_id"
    t.index ["customer_id"], name: "index_customer_orders_on_customer_id"
    t.index ["sales_person_id"], name: "index_customer_orders_on_sales_person_id"
    t.index ["warehouse_id"], name: "index_customer_orders_on_warehouse_id"
  end

  create_table "customer_orders_users", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "customer_order_id", null: false
    t.bigint "user_id", null: false
    t.index ["customer_order_id", "user_id"], name: "index_customer_orders_users_on_customer_order_id_and_user_id"
  end

  create_table "customer_supports", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.float "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.integer "type_of_receipt"
    t.decimal "current_balance", precision: 15, scale: 4, default: "0.0"
    t.decimal "dist_markup", precision: 15, scale: 2
    t.decimal "dist_srp_markup", precision: 15, scale: 2
    t.integer "status"
    t.boolean "admin_credit_hold"
    t.boolean "has_bounced_cheq"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "classification"
    t.string "owner"
    t.integer "salesman_id"
  end

  create_table "discounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "discountable_type"
    t.bigint "discountable_id"
    t.bigint "brand_id"
    t.text "discount"
    t.string "from_month"
    t.string "to_month"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["brand_id"], name: "index_discounts_on_brand_id"
    t.index ["discountable_type", "discountable_id"], name: "index_discounts_on_discountable_type_and_discountable_id"
  end

  create_table "groups", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.json "rights"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups_users", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "group_id", null: false
    t.index ["user_id", "group_id"], name: "index_groups_users_on_user_id_and_group_id"
  end

  create_table "item_bundles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "bundle_set_quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
  end

  create_table "item_images", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "item_id"
    t.boolean "is_primary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_item_images_on_item_id"
  end

  create_table "item_inventories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "item_id"
    t.string "serial_id"
    t.integer "added_by"
    t.integer "item_status"
    t.integer "stock_section_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_item_inventories_on_item_id"
  end

  create_table "items", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "brand_id"
    t.string "name"
    t.text "details"
    t.boolean "is_bundle"
    t.string "sku"
    t.decimal "price", precision: 15, scale: 4
    t.decimal "dealer_price", precision: 15, scale: 4
    t.decimal "srp_price", precision: 15, scale: 4
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.integer "category_id"
    t.boolean "is_identifiable", default: false
    t.index ["brand_id"], name: "index_items_on_brand_id"
  end

  create_table "order_slips", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.integer "warehouse_id"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "orderable_id"
    t.string "orderable_type"
  end

  create_table "ordered_items", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.integer "order_slip_id"
    t.integer "item_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "permission_code"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions_users", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "permission_id", null: false
    t.index ["permission_id", "user_id"], name: "index_permissions_users_on_permission_id_and_user_id"
    t.index ["user_id", "permission_id"], name: "index_permissions_users_on_user_id_and_permission_id"
  end

  create_table "postal_codes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "receivable_breakdowns", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "receivable_id"
    t.bigint "customer_address_id"
    t.integer "gross_sales_cents", default: 0, null: false
    t.string "gross_sales_currency", default: "PHP", null: false
    t.integer "net_sales_cents", default: 0, null: false
    t.string "net_sales_currency", default: "PHP", null: false
    t.integer "net_amount_due_cents", default: 0, null: false
    t.string "net_amount_due_currency", default: "PHP", null: false
    t.date "start_date"
    t.date "end_date"
    t.json "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_address_id"], name: "index_receivable_breakdowns_on_customer_address_id"
    t.index ["receivable_id"], name: "index_receivable_breakdowns_on_receivable_id"
  end

  create_table "receivables", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "sys_ref_number"
    t.string "cust_ref_number"
    t.integer "gross_sales_cents", default: 0, null: false
    t.string "gross_sales_currency", default: "PHP", null: false
    t.integer "net_sales_cents", default: 0, null: false
    t.string "net_sales_currency", default: "PHP", null: false
    t.integer "net_amount_due_cents", default: 0, null: false
    t.string "net_amount_due_currency", default: "PHP", null: false
    t.date "process_date"
    t.date "start_date"
    t.date "end_date"
    t.json "doc_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "template", default: "default"
    t.bigint "customer_address_id"
    t.index ["customer_address_id"], name: "index_receivables_on_customer_address_id"
    t.index ["customer_id"], name: "index_receivables_on_customer_id"
  end

  create_table "return_details", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "item_inventory_id"
    t.integer "return_status"
    t.date "returned_at"
    t.bigint "return_slip_id"
    t.index ["item_inventory_id"], name: "index_return_details_on_item_inventory_id"
    t.index ["return_slip_id"], name: "index_return_details_on_return_slip_id"
  end

  create_table "return_slips", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "item_inventory_serial"
    t.text "description"
    t.text "reasons"
    t.integer "total_quantity"
    t.decimal "unit_price", precision: 15, scale: 2
    t.decimal "price", precision: 15, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "item_id"
  end

  create_table "role_permissions", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "role_id"
    t.bigint "permission_id"
    t.index ["permission_id"], name: "index_role_permissions_on_permission_id"
    t.index ["role_id"], name: "index_role_permissions_on_role_id"
  end

  create_table "roles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "role_code"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sales_details", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "item_inventory_id"
    t.date "sold_at"
    t.integer "sold_to"
    t.integer "sold_by"
    t.bigint "customer_order_item_id", null: false
    t.index ["item_inventory_id"], name: "index_sales_details_on_item_inventory_id"
  end

  create_table "salesmen", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "phone_1"
    t.string "phone_2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "state_or_provinces", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stock_sections", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "warehouse_id"
    t.string "name"
    t.string "sc_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["warehouse_id"], name: "index_stock_sections_on_warehouse_id"
  end

  create_table "sub_categories_items", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "category_id"
    t.bigint "item_id"
    t.index ["category_id"], name: "index_sub_categories_items_on_category_id"
    t.index ["item_id"], name: "index_sub_categories_items_on_item_id"
  end

  create_table "suburbs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "supplier_addresses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "supplier_id"
    t.boolean "is_primary"
    t.boolean "is_business_address"
    t.boolean "is_home_address"
    t.boolean "is_delivery_address"
    t.string "fax"
    t.string "delivery_schedule_day"
    t.string "delivery_schedule_time"
    t.string "business_name"
    t.string "tax_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["supplier_id"], name: "index_supplier_addresses_on_supplier_id"
  end

  create_table "supplier_brand_discounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "supplier_id"
    t.bigint "brand_id"
    t.string "name"
    t.decimal "discount", precision: 15, scale: 4
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["brand_id"], name: "index_supplier_brand_discounts_on_brand_id"
    t.index ["supplier_id"], name: "index_supplier_brand_discounts_on_supplier_id"
  end

  create_table "supplier_discounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "supplier_id"
    t.string "name"
    t.decimal "discount", precision: 15, scale: 4
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["supplier_id"], name: "index_supplier_discounts_on_supplier_id"
  end

  create_table "suppliers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "tax_id"
    t.decimal "current_balance", precision: 15, scale: 4, default: "0.0"
    t.integer "status"
    t.boolean "auto"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "town_or_cities", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_user_roles_on_role_id"
    t.index ["user_id"], name: "index_user_roles_on_user_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email"
    t.string "encrypted_password", default: "", null: false
    t.string "username", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.integer "warehouse_id", default: 0
    t.integer "warehose_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "warehouses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "wh_code"
    t.string "wh_address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "customer_orders", "users", column: "sales_person_id"
  add_foreign_key "receivables", "customer_addresses"
end
