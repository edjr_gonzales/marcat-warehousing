# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

default_permissions = ['allow_add', 'allow_edit', 'allow_delete']

role_permissions = {
  root: ['allow_root'],
  users: default_permissions,
  customers: default_permissions,
  customer_addresses: default_permissions,
  customer_discounts: default_permissions,
  customer_contact_information: default_permissions,
  suppliers: default_permissions,
  supplier_addresses: default_permissions,
  supplier_discounts: default_permissions,
  supplier_contact_information: default_permissions,
  salesmen: default_permissions.push('allow_sell'),
  items: default_permissions,
  item_inventories: default_permissions,
  categories: default_permissions,
  sub_categories: default_permissions,
  brands: default_permissions,
  warehouses: default_permissions,
  stock_sections: default_permissions,
  order_slips: default_permissions,
  item_bundles: default_permissions,
  return_slips: ['allow_add']
}

role_permissions.each do |role_code, permission_codes|
  role_code_s = role_code.to_s

  role = Role.create_with(
    name: role_code_s.titleize,
    description: role_code_s.humanize
  ).find_or_create_by(role_code: role_code_s)

  permission_codes.each do |permission_code|
    permission = Permission.create_with(
      name: "#{role_code_s.titleize} #{permission_code.titleize}",
      description: "#{role_code_s.humanize} #{permission_code.humanize}"
    ).find_or_create_by(permission_code: "#{role_code_s}_#{permission_code}")

    role.permissions << permission unless role.permissions.include?(permission)
  end
end

unless User.any?
  user = User.create!({
    username: 'root_admin',
    email: 'root_admin@test.com',
    password: 'P@55w0rd',
    status: :active
  })

  user.roles << Role.find_by(role_code: 'root')
  user.permissions << Permission.find_by(permission_code: 'root_allow_root')
end

unless ContactInformation.any?
  ContactInformation.create!({
    name: 'Root Admin',
    contactable_id: 1,
    contactable_type: 'User'
  })
end
