class CreateCustomerAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_addresses do |t|
      t.belongs_to :customer
      t.boolean :is_primary
      t.boolean :is_business_address
      t.boolean :is_home_address
      t.string :contact_person
      t.string :phone
      t.string :fax

      t.timestamps
    end
  end
end
