class AddSalesmanIdToCustomer < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :salesman_id, :integer
  end
end
