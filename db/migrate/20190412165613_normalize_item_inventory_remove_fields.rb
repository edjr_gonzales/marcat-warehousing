class NormalizeItemInventoryRemoveFields < ActiveRecord::Migration[5.2]
  def up
    remove_column :item_inventories, :sold_at
    remove_column :item_inventories, :sales_delivery_receipt
    remove_column :item_inventories, :sales_invoice_receipt
    remove_column :item_inventories, :return_status
    remove_column :item_inventories, :returned_at
    remove_column :item_inventories, :return_slip_id

    add_column :sales_details, :sold_by, :integer
  end

  def down
    add_column :item_inventories, :sold_at, :datetime
    add_column :item_inventories, :sales_delivery_receipt, :integer
    add_column :item_inventories, :sales_invoice_receipt, :integer
    add_column :item_inventories, :return_status, :integer
    add_column :item_inventories, :returned_at, :datetime
    add_column :item_inventories, :return_slip_id, :integer

    remove_column :sales_details, :sold_by
  end
end
