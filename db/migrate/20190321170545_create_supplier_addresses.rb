class CreateSupplierAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :supplier_addresses do |t|
    	t.belongs_to :supplier
    	t.boolean :is_primary
    	t.boolean :is_business_address
    	t.boolean :is_home_address
    	t.boolean :is_delivery_address
    	t.string :fax
    	t.string :delivery_schedule_day
    	t.string :delivery_schedule_time
    	t.string :business_name
    	t.string :tax_id

      t.timestamps
    end
  end
end
