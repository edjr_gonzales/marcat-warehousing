class AddOrderableToOrderSlip < ActiveRecord::Migration[5.2]
  def change
  	add_column :order_slips, :orderable_id, :integer
  	add_column :order_slips, :orderable_type, :string
  end
end
