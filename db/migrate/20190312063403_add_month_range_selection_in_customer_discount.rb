class AddMonthRangeSelectionInCustomerDiscount < ActiveRecord::Migration[5.2]
  def change
  	add_column :customer_discounts, :from_month, :integer
  	add_column :customer_discounts, :to_month, :integer
  end
end
