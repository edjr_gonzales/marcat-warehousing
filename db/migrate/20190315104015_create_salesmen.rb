class CreateSalesmen < ActiveRecord::Migration[5.2]
  def change
    create_table :salesmen do |t|
      t.string :name
      t.string :phone_1
      t.string :phone_2

      t.timestamps
    end
  end
end
