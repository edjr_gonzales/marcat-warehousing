class FixFields < ActiveRecord::Migration[5.2]
  def change
    remove_column :collection_informations, :contact_person, :string
    remove_column :collection_informations, :contact_person_2, :string
    remove_column :collection_informations, :tel_no, :string
    remove_column :collection_informations, :tel_no_2, :string

    remove_column :customer_addresses, :contact_person, :string
    remove_column :customer_addresses, :contact_person_2, :string
    remove_column :customer_addresses, :phone, :string
    remove_column :customer_addresses, :phone_2, :string

    remove_column :customers, :business_name, :string
    remove_column :customers, :tax_id, :string
    remove_column :customers, :payment_term, :integer
    remove_column :customers, :credit_limit, :decimal, precision: 15, scale: 4

    add_column :customer_addresses, :business_name, :string
    add_column :customer_addresses, :tax_id, :string
  end
end
