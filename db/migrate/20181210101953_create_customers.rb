class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :email
      t.string :tax_id
      t.integer :payment_term
      t.integer :delivery_pref
      t.decimal :credit_limit, precision: 15, scale: 4
      t.decimal :current_balance, precision: 15, scale: 4
      t.decimal :dist_markup, precision: 15, scale: 2
      t.decimal :dist_srp_markup, precision: 15, scale: 2
      t.integer :status
      t.boolean :admin_credit_hold
      t.boolean :has_bounced_cheq

      t.timestamps
    end
  end
end
