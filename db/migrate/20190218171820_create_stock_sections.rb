class CreateStockSections < ActiveRecord::Migration[5.1]
  def change
    create_table :stock_sections do |t|
      t.belongs_to :warehouse
      t.string :name
      t.string :sc_code

      t.timestamps
    end
  end
end
