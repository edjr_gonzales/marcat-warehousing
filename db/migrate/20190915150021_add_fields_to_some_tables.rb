class AddFieldsToSomeTables < ActiveRecord::Migration[5.2]
  def self.up
    add_column :users, :warehouse_id, :integer, default: 0
    add_column :items, :is_identifiable, :boolean, default: false
  end

  def self.down
    remove_column :items, :is_identifiable
    remove_column :users, :warehouse_id
  end
end
