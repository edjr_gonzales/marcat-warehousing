class AddStatusFieldToWarehouses < ActiveRecord::Migration[5.2]
  def change
    add_column :warehouses, :status, :integer
  end
end
