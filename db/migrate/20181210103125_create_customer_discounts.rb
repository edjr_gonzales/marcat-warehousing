class CreateCustomerDiscounts < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_discounts do |t|
      t.belongs_to :customer
      t.string :name
      t.float :discount

      t.timestamps
    end
  end
end
