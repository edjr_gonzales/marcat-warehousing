class CreateCustomerOrderJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :customer_orders, :users do |t|
    	t.index [:customer_order_id, :user_id]
    end
  end
end
