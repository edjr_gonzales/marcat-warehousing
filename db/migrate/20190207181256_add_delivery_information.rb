class AddDeliveryInformation < ActiveRecord::Migration[5.1]
  def change
    add_column :customer_addresses, :is_delivery_address, :boolean
    add_column :customer_addresses, :delivery_schedule, :string, default: nil
  end
end
