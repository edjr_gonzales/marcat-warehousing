class CreateRolePermission < ActiveRecord::Migration[5.1]
  def change
    create_table :role_permissions, id: false do |t|
    	t.belongs_to :role
    	t.belongs_to :permission
    end
  end
end
