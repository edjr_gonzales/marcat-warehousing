class CreateCustomerSupports < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_supports do |t|
      t.string :name
      t.float :amount

      t.timestamps
    end
  end
end
