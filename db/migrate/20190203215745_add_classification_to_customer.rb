class AddClassificationToCustomer < ActiveRecord::Migration[5.1]
  def change
  	add_column :customers, :classification, :integer
  end
end
