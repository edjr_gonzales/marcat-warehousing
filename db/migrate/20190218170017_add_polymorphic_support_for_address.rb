class AddPolymorphicSupportForAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :addresses, :addressable_id, :integer
    add_column :addresses, :addressable_type, :string

    add_column :addresses, :basic_address, :string
  end
end
