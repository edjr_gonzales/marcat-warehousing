class CreateStateOrProvinces < ActiveRecord::Migration[5.1]
  def change
    create_table :state_or_provinces do |t|
      t.string :name

      t.timestamps
    end
  end
end
