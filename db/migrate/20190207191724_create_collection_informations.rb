class CreateCollectionInformations < ActiveRecord::Migration[5.1]
  def change
    create_table :collection_informations do |t|
      t.belongs_to :customer
      t.string :contact_person
      t.string :counter_day_time
      t.string :collection_day_time
      t.string :tel_no
      t.integer :credit_term
      t.decimal :credit_limit, precision: 15, scale: 4

      t.timestamps
    end
  end
end
