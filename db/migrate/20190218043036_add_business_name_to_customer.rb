class AddBusinessNameToCustomer < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :business_name, :string
  end
end
