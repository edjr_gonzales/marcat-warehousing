class CreateTransactionReceiptModels < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_orders do |t|
    	t.references :customer
      t.references :warehouse
      t.references :sales_person, index: true, foreign_key: { to_table: :users }

      t.integer    :order_slip_id, default: 0
      t.integer    :shipping_address_id
      t.integer    :billing_address_id
      t.string     :trans_ref_number
    	t.monetize   :gross_sales
    	t.monetize   :net_sales
    	t.monetize   :net_amount_due
      t.date       :delivery_date
      t.boolean    :destroyed, default: false
    	t.json       :doc_data

      t.timestamps
    end

    create_table :customer_order_items do |t|
      t.references :customer_order
      t.references :item
      t.float      :total_discount, default: 0
      t.monetize   :total_amount
      t.json       :sales_data

    end

    create_table :customer_order_statuses do |t|
      t.references :customer_orders
      t.integer    :order_status
      t.string     :status_detail, limit: 512
      t.timestamps
    end

    change_column :customer_orders, :id, :bigint, unique: true, null: false, auto_increment: true
    change_column :customer_order_items, :id, :bigint, unique: true, null: false, auto_increment: true
    change_column :customer_order_statuses, :id, :bigint, unique: true, null: false, auto_increment: true
    change_column :sales_details, :id, :bigint, unique: true, null: false, auto_increment: true

    remove_column :sales_details, :sales_delivery_receipt_id
    remove_column :sales_details, :sales_invoice_receipt_id
    add_column    :sales_details, :customer_order_item_id, :bigint, null: false
  end
end
