class AddDefaultValueOfCurrentBalanceToCustomer < ActiveRecord::Migration[5.1]
  def change
  	change_column :customers, :current_balance, :decimal, precision: 15, scale: 4, default: 0.00
  end
end
