class CreateActivityLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :activity_logs do |t|
      t.references :user_accountable, polymorphic: true, index: { name: 'activity_logs_index' }
      t.belongs_to :user
      t.string :log_type
      t.string :log_message
      t.integer :updated_by

      t.timestamps
    end
  end
end
