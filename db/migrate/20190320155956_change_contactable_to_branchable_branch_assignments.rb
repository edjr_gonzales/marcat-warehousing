class ChangeContactableToBranchableBranchAssignments < ActiveRecord::Migration[5.2]
  def change
  	rename_column :branch_assignments, :contactable_id, :branchable_id
  	rename_column :branch_assignments, :contactable_type, :branchable_type
  end
end
