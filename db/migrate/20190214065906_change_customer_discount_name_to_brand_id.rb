class ChangeCustomerDiscountNameToBrandId < ActiveRecord::Migration[5.1]
  def change
  	remove_column :customer_discounts, :name
    add_column :customer_discounts, :brand_id, :integer
  end
end
