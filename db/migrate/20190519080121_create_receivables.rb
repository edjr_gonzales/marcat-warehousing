class CreateReceivables < ActiveRecord::Migration[5.2]
  def change
    create_table :receivables do |t|
    	t.references :customer
      t.string     :sys_ref_number
      t.string     :cust_ref_number, null: true
    	t.monetize   :gross_sales
    	t.monetize   :net_sales
    	t.monetize   :net_amount_due
    	t.date       :process_date
      t.date       :start_date
      t.date       :end_date
    	t.json       :doc_data

      t.timestamps
    end

    create_table :receivable_breakdowns do |t|
      t.references :receivable
      t.references :customer_address

      t.monetize   :gross_sales
      t.monetize   :net_sales
      t.monetize   :net_amount_due

      t.date       :start_date
      t.date       :end_date
      t.json       :data

      t.timestamps
    end
  end
end
