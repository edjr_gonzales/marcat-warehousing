class CreateWarehouses < ActiveRecord::Migration[5.1]
  def change
    create_table :warehouses do |t|
      t.string :name
      t.string :wh_code
      t.string :wh_address

      t.timestamps
    end
  end
end
