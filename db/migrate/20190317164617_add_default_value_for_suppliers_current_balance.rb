class AddDefaultValueForSuppliersCurrentBalance < ActiveRecord::Migration[5.2]
  def change
	  change_column_default :suppliers, :current_balance, '0.0'
  end
end
