class NormalizeItemInventories < ActiveRecord::Migration[5.2]
  def up
    create_table :sales_details do |t|
      t.belongs_to :item_inventory
      t.date :sold_at
      t.integer :sold_to # customer_id
      t.integer :sales_delivery_receipt_id
      t.integer :sales_invoice_receipt_id
    end

    create_table :return_details do |t|
      t.belongs_to :item_inventory
      t.integer :return_status
      t.date :returned_at
      t.belongs_to :return_slip
    end
  end

  def down
    drop_table :sales_details
    drop_table :return_details
  end
end
