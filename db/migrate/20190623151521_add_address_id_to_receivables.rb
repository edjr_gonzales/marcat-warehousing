class AddAddressIdToReceivables < ActiveRecord::Migration[5.2]
  def change
    add_reference :receivables, :customer_address, foreign_key: true
  end
end
