class ChangeDiscountTypeToString < ActiveRecord::Migration[5.1]
  def change
  	change_column :customer_discounts, :discount, :string
  end
end
