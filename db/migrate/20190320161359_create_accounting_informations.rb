class CreateAccountingInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :accounting_informations do |t|
    	t.belongs_to :supplier
    	t.integer :credit_term

      t.timestamps
    end

    add_column :contact_informations, :email, :string
  end
end
