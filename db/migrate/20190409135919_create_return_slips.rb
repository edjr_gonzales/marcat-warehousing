class CreateReturnSlips < ActiveRecord::Migration[5.2]
  def change
    create_table :return_slips do |t|
    	t.string :item_code
    	t.string :item_inventory_serial
    	t.text :description
    	t.text :reasons
    	t.integer :total_quantity
    	t.decimal :unit_price, precision: 15, scale: 2
    	t.decimal :price, precision: 15, scale: 2

      t.timestamps
    end
  end
end
