class AddSubCategoryFieldToItems < ActiveRecord::Migration[5.2]
  def change
    create_table :sub_categories_items, id: false do |t|
      t.belongs_to :category
      t.belongs_to :item
    end
  end
end
