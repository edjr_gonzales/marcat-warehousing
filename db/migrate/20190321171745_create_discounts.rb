class CreateDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :discounts do |t|
    	t.references :discountable, polymorphic: true
    	t.belongs_to :brand
    	t.text :discount
    	t.integer :from_month
    	t.integer :to_month

      t.timestamps
    end
  end
end
