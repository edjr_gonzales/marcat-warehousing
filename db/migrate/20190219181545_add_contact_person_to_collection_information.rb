class AddContactPersonToCollectionInformation < ActiveRecord::Migration[5.2]
  def change
    add_column :collection_informations, :contact_person_2, :string, null: true
    add_column :collection_informations, :tel_no_2, :string, null: true
  end
end
