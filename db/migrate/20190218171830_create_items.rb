class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.belongs_to :brand
      t.string :name
      t.text :details
      t.boolean :is_bundle
      t.string :sku
      t.decimal :price, precision: 15, scale: 4
      t.decimal :dealer_price, precision: 15, scale: 4
      t.decimal :srp_price, precision: 15, scale: 4

      t.timestamps
    end
  end
end
