class AddFieldsToCustomerOrders < ActiveRecord::Migration[5.2]
  def self.up
    remove_monetize :customer_order_items, :total_amount
    remove_column :customer_orders, :destroyed
    add_monetize :customer_order_items, :net_amount
    add_monetize :customer_order_items, :gross_amount
    add_column :customer_orders, :sale_date, :date, null: true
    add_column :customer_orders, :saved, :boolean, default: false
    add_column :customer_orders, :po_reference, :string, default: ''
    add_column :customer_orders, :removed, :boolean, default: false
    add_column :customer_orders, :removed_at, :datetime
    add_column :customer_orders, :salesman_id, :integer

    create_table :customer_order_checkers do |t|
      t.references :customer_order
      t.references :user
    end
  end

  def self.down
    drop_table :customer_order_checkers

    remove_column :customer_orders, :salesman_id
    remove_column :customer_orders, :removed_at
    remove_column :customer_orders, :removed
    remove_column :customer_orders, :po_reference
    remove_column :customer_orders, :saved
    remove_column :customer_orders, :sale_date
    remove_monetize :customer_order_items, :gross_amount
    remove_monetize :customer_order_items, :net_amount
    add_column :customer_orders, :destroyed, :boolean
    add_monetize :customer_order_items, :total_amount
  end
end
