class CreateSupplierBrandDiscounts < ActiveRecord::Migration[5.1]
  def change
    create_table :supplier_brand_discounts do |t|
      t.belongs_to :supplier
      t.belongs_to :brand
      t.string :name, null: true
      t.decimal :discount, precision: 15, scale: 4

      t.timestamps
    end
  end
end
