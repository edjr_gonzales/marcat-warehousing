class CreateContactInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :contact_informations do |t|
    	t.belongs_to :customer
      t.string :name
      t.string :contact_number_1
      t.string :contact_number_2

      t.timestamps
    end
  end
end
