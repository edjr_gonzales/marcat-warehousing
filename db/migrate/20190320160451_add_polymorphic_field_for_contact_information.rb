class AddPolymorphicFieldForContactInformation < ActiveRecord::Migration[5.2]
  def change
  	add_column :contact_informations, :contactable_id, :integer
  	add_column :contact_informations, :contactable_type, :string
  	remove_column :contact_informations, :customer_id
  end
end
