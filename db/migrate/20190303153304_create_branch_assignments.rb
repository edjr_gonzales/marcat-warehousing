class CreateBranchAssignments < ActiveRecord::Migration[5.2]
  def change
    create_table :branch_assignments do |t|
    	t.references :contactable, polymorphic: true, index: { name: 'contactable_index' }
    	t.belongs_to :contact_information

      t.timestamps
    end
  end
end
