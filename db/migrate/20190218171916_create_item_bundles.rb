class CreateItemBundles < ActiveRecord::Migration[5.1]
  def change
    create_table :item_bundles do |t|
      t.integer :bundled_item_id
      t.integer :parent_item_id
      t.integer :item_quantity

      t.timestamps
    end
  end
end
