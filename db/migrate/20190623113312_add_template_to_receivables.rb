class AddTemplateToReceivables < ActiveRecord::Migration[5.2]
  def change
    add_column :receivables, :template, :string, default: 'default'
  end
end
