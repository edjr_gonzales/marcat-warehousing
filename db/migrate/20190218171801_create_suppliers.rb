class CreateSuppliers < ActiveRecord::Migration[5.1]
  def change
    create_table :suppliers do |t|
      t.string :name
      t.string :email
      t.string :tax_id
      t.integer :payment_term
      t.decimal :current_balance, precision: 15, scale: 4
      t.decimal :dist_markup, precision: 15, scale: 4
      t.decimal :dist_srp_markup, precision: 15, scale: 4
      t.integer :status
      t.boolean :auto

      t.timestamps
    end
  end
end
