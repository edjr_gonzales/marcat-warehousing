class CreateItemImages < ActiveRecord::Migration[5.1]
  def change
    create_table :item_images do |t|
      t.belongs_to :item
      t.boolean :is_primary

      t.timestamps
    end
  end
end
