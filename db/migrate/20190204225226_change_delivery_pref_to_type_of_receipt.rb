class ChangeDeliveryPrefToTypeOfReceipt < ActiveRecord::Migration[5.1]
  def change
  	rename_column :customers, :delivery_pref, :type_of_receipt
  end
end
