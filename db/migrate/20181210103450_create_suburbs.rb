class CreateSuburbs < ActiveRecord::Migration[5.1]
  def change
    create_table :suburbs do |t|
      t.string :code

      t.timestamps
    end
  end
end
