class CreateTownOrCities < ActiveRecord::Migration[5.1]
  def change
    create_table :town_or_cities do |t|
      t.string :name

      t.timestamps
    end
  end
end
