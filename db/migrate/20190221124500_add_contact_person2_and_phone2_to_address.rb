class AddContactPerson2AndPhone2ToAddress < ActiveRecord::Migration[5.2]
  def change
  	add_column :customer_addresses, :contact_person_2, :string, null: true
  	add_column :customer_addresses, :phone_2, :string, null: true
  end
end
