class FixUnnecessaryFields < ActiveRecord::Migration[5.2]
  def up
    remove_column :item_bundles, :bundled_item_id
    remove_column :item_bundles, :parent_item_id
    remove_column :item_inventories, :bundled_to
    remove_column :suppliers, :payment_term
    remove_column :suppliers, :dist_markup
    remove_column :suppliers, :dist_srp_markup

    # change_column :discounts, :from_month, :date
    # change_column :discounts, :to_month, :date

    rename_column :item_inventories, :return_slip, :return_slip_id
    rename_column :item_inventories, :item_loc, :stock_section_id
    rename_column :item_bundles, :item_quantity, :bundle_set_quantity
  end

  def down
    add_column :item_bundles, :bundled_item_id, :integer
    add_column :item_bundles, :parent_item_id, :integer
    add_column :item_inventories, :bundled_to, :integer
    add_column :suppliers, :payment_term, :integer
    add_column :suppliers, :dist_markup, :decimal, precision: 15, scale: 4
    add_column :suppliers, :dist_srp_markup, :decimal, precision: 15, scale: 4

    # change_column :discounts, :from_month, :integer
    # change_column :discounts, :to_month, :integer

    rename_column :item_inventories, :return_slip_id, :return_slip
    rename_column :item_inventories, :stock_section_id, :item_loc
    rename_column :item_bundles, :bundle_set_quantity, :item_quantity
  end
end
