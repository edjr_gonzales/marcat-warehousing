class CreateBundledItems < ActiveRecord::Migration[5.2]
  def change
    create_table :bundled_items do |t|
    	t.belongs_to :item
    	t.belongs_to :item_bundle
    	t.integer :quantity

      t.timestamps
    end

    drop_join_table :items, :item_bundles do |t|
      t.index [:item_id, :item_bundle_id]
      t.index [:item_bundle_id, :item_id]
    end

    drop_join_table :items, :categories do |t|
    	t.index [:item_id, :category_id]
    end

    add_column :item_bundles, :name, :string
  end
end
