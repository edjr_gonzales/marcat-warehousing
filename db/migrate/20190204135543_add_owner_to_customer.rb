class AddOwnerToCustomer < ActiveRecord::Migration[5.1]
  def change
  	add_column :customers, :owner, :string
  end
end
