class CreateItemBundlesJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :items, :item_bundles do |t|
      t.index [:item_id, :item_bundle_id]
      t.index [:item_bundle_id, :item_id]
    end
  end
end
