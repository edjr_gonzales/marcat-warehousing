class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
    	t.belongs_to :customer_address
    	t.string :apt_bldg_suite
			t.string :unit 
			t.string :floor
			t.string :lot_no
			t.string :street
			t.string :brgy
			t.string :district
			t.string :town_city
			t.string :postal_code
			t.string :state_province
			t.string :country

      t.timestamps
    end
  end
end
