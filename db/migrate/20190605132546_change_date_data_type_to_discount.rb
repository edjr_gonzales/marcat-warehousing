class ChangeDateDataTypeToDiscount < ActiveRecord::Migration[5.2]
  def change
  	change_column :discounts, :from_month, :string
  	change_column :discounts, :to_month, :string
  end
end
