class ReplaceItemCodeWithItemIdFieldFromReturnSlips < ActiveRecord::Migration[5.2]
  def change
  	add_column :return_slips, :item_id, :integer
  	remove_column :return_slips, :item_code
  end
end
