class CreateItemInventories < ActiveRecord::Migration[5.1]
  def change
    create_table :item_inventories do |t|
      t.belongs_to :item
      t.string :serial_id
      t.integer :added_by
      t.integer :item_status
      t.datetime :sold_at
      t.integer :sales_delivery_receipt
      t.integer :sales_invoice_receipt
      t.integer :return_status
      t.datetime :returned_at
      t.integer :return_slip, null: true
      t.integer :item_loc
      t.integer :bundled_to, null: true

      t.timestamps
    end
  end
end
