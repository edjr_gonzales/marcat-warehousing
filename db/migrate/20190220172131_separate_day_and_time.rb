class SeparateDayAndTime < ActiveRecord::Migration[5.2]
  def change
    remove_column :collection_informations, :collection_day_time
    add_column :collection_informations, :collection_day, :string
    add_column :collection_informations, :collection_time, :string

    remove_column :collection_informations, :counter_day_time
    add_column :collection_informations, :counter_day, :string
    add_column :collection_informations, :counter_time, :string

    remove_column :customer_addresses, :delivery_schedule
    add_column :customer_addresses, :delivery_schedule_day, :string
    add_column :customer_addresses, :delivery_schedule_time, :string
  end
end
