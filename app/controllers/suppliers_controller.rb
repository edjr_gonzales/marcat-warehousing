class SuppliersController < ApplicationController
  before_action :authenticate_user!
  before_action :sanitize_params, only: [:create, :update]
  before_action :set_supplier, only: [:show, :edit, :update, :archive, :destroy]
  before_action :get_all_brands, only: [:new, :edit]

  # GET /suppliers
  # GET /suppliers.json
  def index
    @suppliers = Supplier.saved

    respond_to do |format|
      format.html
      format.json {
        render json: SupplierDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /suppliers/1
  # GET /suppliers/1.json
  def show
  end

  # GET /suppliers/new
  def new
    authorize Supplier, :allow_add?

    if Supplier.unsaved.present?
      @supplier = Supplier.unsaved.first
    else
      @supplier = Supplier.new(status: :unsaved)

      @supplier.save(validate: false)

      @accounting_information = AccountingInformation.new
      @accounting_information.supplier = @supplier
      @accounting_information.save(validate: false)

      log_transaction(@supplier)
    end
  end

  # GET /suppliers/1/edit
  def edit
    authorize Supplier, :allow_edit?
  end

  # POST /suppliers
  # POST /suppliers.json
  def create
    authorize Supplier, :allow_add?
  end

  # PATCH/PUT /suppliers/1
  # PATCH/PUT /suppliers/1.json
  def update
    authorize Supplier, :allow_edit?

    @contact_information = ContactInformation.find_by(id: accounting_information_params[:contact_info])

    respond_to do |format|
      if @supplier.accounting_information.present?
        @supplier.accounting_information.set_contact(@contact_information) if @contact_information.present?
        @supplier.accounting_information.update(accounting_information_params)
      else
        accounting_info = AccountingInformation.new(accounting_information_params)
        accounting_info.supplier = @supplier
        accounting_info.set_contact(@contact_information)
        accounting_info.save
      end

      @supplier.status = :_new if @supplier.unsaved?

      if @supplier.update(supplier_params)
        log_transaction(@supplier)

        format.html { redirect_to @supplier, notice: 'Supplier was successfully updated.' }
        format.json { render :show, status: :ok, location: @supplier }
      else
        format.html { render :edit }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /suppliers/1
  # DELETE /suppliers/1.json
  def destroy
    authorize Supplier, :allow_delete?

    @supplier.destroy

    log_transaction(@supplier)

    respond_to do |format|
      format.html { redirect_to suppliers_url, notice: "Supplier was successfully #{params[:axn] || 'destroyed'}." }
      format.json { head :no_content }
    end
  end

  # Archive suppliers
  def archive
    authorize Supplier, :allow_delete?

    respond_to do |format|
      @supplier.archived!

      format.html { redirect_to suppliers_url, notice: "Supplier was successfully #{params[:axn] || 'archived'}." }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_supplier
    @supplier = Supplier.find(params[:id])
  end

  def get_all_brands
    @brands = Brand.all
  end

  def sanitize_params
    sanitize_enum_params(:supplier, %w(payment_term))
    sanitize_enum_params(:accounting_information, %w(credit_term))
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def supplier_params
    params.require(:supplier).permit(:name, :email, :tax_id, :credit_terms, :current_balance, :auto)
  end

  def accounting_information_params
    params.require(:accounting_information).permit(:credit_term, :credit_limit, :contact_info)
  end
end
