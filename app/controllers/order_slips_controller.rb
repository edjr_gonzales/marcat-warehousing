class OrderSlipsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_order_slip, only: [:show, :edit, :update, :destroy]
  before_action :set_items, only: [:new, :create, :edit, :update]

  # GET /order_slips
  # GET /order_slips.json
  def index
    @order_slips = OrderSlip.all

    respond_to do |format|
      format.html
      format.json {
        render json: OrderSlipDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /order_slips/1
  # GET /order_slips/1.json
  def show
  end

  # GET /order_slips/new
  def new
    @order_slip = OrderSlip.new
  end

  # GET /order_slips/1/edit
  def edit
  end

  # POST /order_slips
  # POST /order_slips.json
  def create
    @order_slip = OrderSlip.new(order_slip_params)

    respond_to do |format|
      items = params[:items]

      if items.uniq.length == items.length
        is_valid = is_breakdown_valid?

        binding.pry

        if is_valid
          @order_slip.status = :_new

          if @order_slip.save
            log_transaction(@order_slip)

            save_breakdown

            format.html { redirect_to @order_slip, notice: 'Item bundle was successfully created.' }
            format.json { render :show, status: :created, location: @order_slip }
          else
            format.html { render :new }
            format.json { render json: @order_slip.errors, status: :unprocessable_entity }
          end
        else
          format.html { render :new }
          format.json { render json: @order_slip.errors, status: :unprocessable_entity }
        end
      else
        @order_slip.errors.add(:base, 'There are duplicate items selected')

        format.html { render :new }
        format.json { render json: @order_slip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /order_slips/1
  # PATCH/PUT /order_slips/1.json
  def update
    respond_to do |format|
      if @order_slip.update(order_slip_params)
        log_transaction(@order_slip)

        format.html { redirect_to @order_slip, notice: 'Order Slip was successfully updated.' }
        format.json { render :show, status: :ok, location: @order_slip }
      else
        format.html { render :edit }
        format.json { render json: @order_slip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /order_slips/1
  # DELETE /order_slips/1.json
  def destroy
    @order_slip.destroy

    log_transaction(@order_slip)

    respond_to do |format|
      format.html { redirect_to order_slips_url, notice: 'Order Slip was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_order_slip
    @order_slip = OrderSlip.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_slip_params
    params.require(:order_slip).permit(:orderable_id, :warehouse_id, :orderable_type)
  end

  def set_items
    @items = Item.saved
  end

  def is_breakdown_valid?
    # check all items' inventories if quantities are valid
    is_valid = true
    invalid_item = { name: '', current: 0, requested: 0 }

    params[:items].each_with_index do |id, i|
      next if id.blank?

      item = Item.find(id)
      quantity = params[:item_quantity][i].to_i

      if item.item_inventories.stock.count < quantity.to_i
        is_valid = false
        @order_slip.errors.add(:base, "Item <strong>#{item.name}</strong> inventories are insufficient. <br /><strong>Current inventory:</strong> #{item.item_inventories.stock.count} <br /><strong>Requested:</strong> #{quantity}")
        break
      end
    end
  end

  def save_breakdown
    @order_slip.ordered_items.destroy_all

    params[:items].each_with_index do |id, i|
      next if id.blank?

      quantity = params[:item_quantity][i]

      ordered_item = OrderedItem.new
      ordered_item.item_id = id
      ordered_item.order_slip = @order_slip
      ordered_item.quantity = quantity

      ordered_item.save

      log_transaction(ordered_item)
    end
  end
end
