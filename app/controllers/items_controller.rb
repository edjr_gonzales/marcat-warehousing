class ItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_item, only: [:available, :show, :edit, :update, :destroy]
  before_action :set_brands, only: [:new, :create, :edit, :update]
  before_action :sanitize_params, only: [:create, :update]

  # GET /items
  # GET /items.json
  def index
    @items = Item.saved

    respond_to do |format|
      format.html
      format.json {
        render json: ItemDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /items/1
  # GET /items/1.json
  def show
  end

  # GET /items/new
  def new
    authorize Item, :allow_add?

    if Item.unsaved.present?
      @item = Item.unsaved.first
    else
      @item = Item.new(status: :unsaved)

      @item.save(validate: false)

      log_transaction(@item)
    end
  end

  # GET /items/1/edit
  def edit
    authorize Item, :allow_edit?
  end

  # POST /items
  # POST /items.json
  def create
    authorize Item, :allow_add?
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    authorize Item, :allow_edit?

    respond_to do |format|
      @item.status = :_new if @item.unsaved?
      @item.set_category(params[:category_id])
      @item.set_sub_categories(params[:sub_category_ids])
      @item.images.attach(params[:item][:images]) if params[:item][:images].present?

      if @item.update(item_params)
        log_transaction(@item)

        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    authorize Item, :allow_delete?

    @item.destroy

    log_transaction(@item)

    respond_to do |format|
      format.html { redirect_to items_url, notice: "Item was successfully #{params[:axn] || 'destroyed'}." }
      format.json { head :no_content }
    end
  end

  def search_available
    search_handler = ItemSearchHandler.execute(params[:s].strip, ItemInventory.item_statuses[:stock])

    respond_to do |format|
      format.html
      format.json { render json: search_handler.to_json }
    end
  end

  def search_sold
    search_handler = ItemSearchHandler.execute(params[:s].strip, ItemInventory.item_statuses[:sold])

    respond_to do |format|
      format.html
      format.json { render json: search_handler.to_json }
    end
  end

  def search_returned
    search_handler = ItemSearchHandler.execute(params[:s].strip, ItemInventory.item_statuses[:return])

    respond_to do |format|
      format.html
      format.json { render json: search_handler.to_json }
    end
  end

  private
  def set_item
    @item = Item.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_params
    params.require(:item).permit(:id, :brand_id, :name, :details, :is_bundle, :sku, :price, :dealer_price, :srp_price, :category_ids)
  end

  def set_brands
    @brands = Brand.all
  end

  def sanitize_params
    sanitize_formatted_numeric_params(:item, %w(price dealer_price srp_price), 'to_f')
  end
end
