class ReturnSlipsController < ApplicationController
  before_action :authenticate_user!
  before_action :sanitize_params, only: [:create, :update]
  before_action :set_return_slip, only: [:show]

  # GET /return_slips
  # GET /return_slips.json
  def index
    @return_slips = ReturnSlip.all

    respond_to do |format|
      format.html
      format.json {
        render json: ReturnSlipDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /return_slips/1
  # GET /return_slips/1.json
  def show
  end

  # GET /return_slips/new
  def new
    authorize ReturnSlip, :allow_add?

    @return_slip = ReturnSlip.new
    set_selections
  end

  # GET /return_slips/1/edit
  def edit
    raise Pundit::NotAuthorizedError
  end

  # POST /return_slips
  # POST /return_slips.json
  def create
    authorize ReturnSlip, :allow_add?

    @return_slip = ReturnSlip.new(return_slip_params)
    item = Item.find(return_slip_params[:item_id])

    respond_to do |format|
      if return_slip_params[:total_quantity].to_i <= Item.find(params[:return_slip][:item_id]).item_inventories.sold.count
        set_selections

          if @return_slip.save
            item.item_inventories.sold.limit(return_slip_params[:total_quantity]).each do |item_inventory|
              if params[:item_status] == 'Bad'
                item_inventory.return!
              else
                item_inventory.stock!
              end
            end

            log_transaction(@return_slip)

            format.html { redirect_to @return_slip, notice: 'Return Slip was successfully created.' }
            format.json { render :show, status: :created, location: @return_slip }
          else
            format.html { render :new }
            format.json { render json: @return_slip.errors, status: :unprocessable_entity }
          end
      else
        @return_slip.errors.add(:base, "Requested return quantity is greater than the number of sold items. <br /><strong>Requested:</strong> #{return_slip_params[:total_quantity]} <br /><strong>Current Sold Count:</strong> #{item.item_inventories.sold.count}")
        format.html { render :new }
        format.json { render json: @return_slip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /return_slips/1
  # PATCH/PUT /return_slips/1.json
  def update
    raise Pundit::NotAuthorizedError
  end

  # DELETE /return_slips/1
  # DELETE /return_slips/1.json
  def destroy
    @return_slip.destroy

    log_transaction(@return_slip)

    respond_to do |format|
      format.html { redirect_to return_slips_url, notice: 'Return Slip was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_return_slip
    @return_slip = ReturnSlip.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def return_slip_params
    params.require(:return_slip).permit(:item_id, :item_inventory_serial, :total_quantity, :reasons, :unit_price, :price, :description)
  end

  def sanitize_params
    sanitize_formatted_numeric_params(:return_slip, %w(unit_price price), 'to_f')
  end

  def set_selections
    @item = @return_slip.item
    @selection = @item.present? ? @item.item_inventories.sold : []

    @selected = @selection.present? ? @selection.find_by(serial_id: @return_slip.item_inventory_serial) : nil
  end
end
