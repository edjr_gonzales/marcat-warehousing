class StockSectionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_stock_section, only: [:show, :edit, :update, :destroy]
  before_action :set_warehouse, only: [:index, :create, :update]

  # GET /stock-sections
  # GET /stock-sections.json
  def index
    @stock_sections = @warehouse.stock_sections

    respond_to do |format|
      format.json { render json: @stock_sections, status: :ok }
    end
  end

  # GET /stock-sections/1
  # GET /stock-sections/1.json
  def show
  end

  # GET /stock-sections/new
  def new
    @stock_section = StockSection.new
  end

  # GET /stock-sections/1/edit
  def edit
  end

  # POST /stock-sections
  # POST /stock-sections.json
  def create
    @stock_section = StockSection.new(stock_section_params)
    @stock_section.warehouse = @warehouse

    respond_to do |format|
      if @stock_section.save
        log_transaction(@stock_section)

        format.html { redirect_to @stock_section, notice: 'Stock Section was successfully created.' }
        format.json { render :show, status: :created, location: warehouse_stock_section_path(id: @warehouse.id) }
      else
        format.html { render :new }
        format.json { render json: @stock_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stock-sections/1
  # PATCH/PUT /stock-sections/1.json
  def update
    respond_to do |format|
      if @stock_section.update(stock_section_params)
        log_transaction(@stock_section)

        format.html { redirect_to @stock_section, notice: 'Stock Section was successfully updated.' }
        format.json { render :show, status: :ok, location: warehouse_stock_section_path(warehouse_id: @warehouse.id, id: @stock_section) }
      else
        format.html { render :edit }
        format.json { render json: @stock_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stock-sections/1
  # DELETE /stock-sections/1.json
  def destroy
    @stock_section.destroy

    log_transaction(@stock_section)

    respond_to do |format|
      format.html { redirect_to stock_sections_url, notice: 'Stock Section was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_stock_section
    @stock_section = StockSection.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def stock_section_params
    params.require(:stock_section).permit(:name, :sc_code)
  end

  def set_warehouse
    @warehouse = Warehouse.find(params[:warehouse_id])
  end
end
