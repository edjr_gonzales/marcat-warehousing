class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end

  def sanitize_enum_params(resource, paramz)
    paramz.each do |param|
      params[resource][param.to_sym] = params[resource][param.to_sym].to_i
    end
  end

  def sanitize_formatted_numeric_params(resource, paramz, convert_to = 'to_i')
    paramz.each do |param|
      params[resource][param.to_sym] = params[resource][param.to_sym].gsub(',', '').try(convert_to.to_sym)
    end
  end

  def log_transaction(resource, has_model = true)
    message = "#{current_user.username}::#{params[:action]}::#{resource.class.name}::#{resource.id}::#{resource}"

    if has_model
      resource.log(current_user, params[:action], message)
    else
      ActivityLog.log(current_user, params[:action], message)
    end
  end

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action.'
    redirect_to(request.referrer || root_path)
  end
end
