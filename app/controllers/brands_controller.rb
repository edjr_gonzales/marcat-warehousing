class BrandsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_brand, only: [:show, :edit, :update, :destroy]

  # GET /brands
  # GET /brands.json
  def index
    @brands = Brand.all

    respond_to do |format|
      format.html
      format.json {
        render json: BrandDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /brands/1
  # GET /brands/1.json
  def show
  end

  # GET /brands/new
  def new
    authorize Brand, :allow_add?

    @brand = Brand.new
  end

  # GET /brands/1/edit
  def edit
    authorize Brand, :allow_edit?
  end

  # POST /brands
  # POST /brands.json
  def create
    authorize Brand, :allow_add?

    @brand = Brand.new(brand_params)

    respond_to do |format|
      if @brand.save
        log_transaction(@brand)

        format.html { redirect_to @brand, notice: 'Brand was successfully created.' }
        format.json { render :show, status: :created, location: @brand }
      else
        format.html { render :new }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /brands/1
  # PATCH/PUT /brands/1.json
  def update
    authorize Brand, :allow_edit?

    respond_to do |format|
      if @brand.update(brand_params)
        log_transaction(@brand)

        format.html { redirect_to @brand, notice: 'Brand was successfully updated.' }
        format.json { render :show, status: :ok, location: @brand }
      else
        format.html { render :edit }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def destroy
    authorize Brand, :allow_delete?

    @brand.destroy

    log_transaction(@brand)

    respond_to do |format|
      format.html { redirect_to brands_url, notice: 'Brand was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_brand
    @brand = Brand.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def brand_params
    params.require(:brand).permit(:name, :short_code)
  end
end
