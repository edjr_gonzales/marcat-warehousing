require 'receivable_templates/receivable_templates'

class ReceivablesController < ApplicationController

	before_action :fetch_template

	def index
		respond_to do |format|
		    format.html
		    format.json { render json: ReceivableDatatable.new(params, view_context: view_context)	 }
		end
	end

	def new
		customer = params[:customer].present? ? Customer.find_by(id: params[:customer]) : Customer.take
		@receivable = Receivable.new(customer: customer, template: @receivable_tpl)
	end

	def create
		@receivable = Receivable.new(receivable_params)

		respond_to do |format|
			if @receivable.save
				format.html { redirect_to @receivable, notice: 'Receivable was successfully created.' }
        		format.json { render json: { status: :ok, address: @receivable, customer: @receivable.customer } }
			else
				format.html { render :new }
        		format.json { render json: @receivable.errors, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@receivable = Receivable.find(params[:id])
		@receivable_tpl = @receivable.template

		respond_to do |format|
			format.html
			format.json { render json: @receivable }
		end
	end

	def update
		@receivable = Receivable.find(params[:id])

		respond_to do |format|
			if @receivable.update(receivable_params)
				format.html { redirect_to @receivable, notice: 'Receivable was successfully updated.' }
	        	format.json { render :show, status: :ok, location: @receivable }
			else
				format.html { render action: :edit }
	        	format.json { render json: @receivable.errors, status: :unprocessable_entity }
			end
		end
	end

	def show
		@receivable = Receivable.find(params[:id])

		respond_to do |format|
			format.html
			format.json { render json: @receivable }
		end
	end

	def destroy
		@receivable = Receivable.find(params[:id])
		@receivable.destroy

		respond_to do |format|
			if @receivable.destroyed
				format.html { redirect_to action: :index }
				format.json { render json: { status: :ok, receivable: @receivable } }
			else
				format.html { redirect_to action: :index, notice: 'Unable to remove Receivable.' }
				format.json { render json: { status: :unprocessable_entity, receivable: @receivable }, status: :unprocessable_entity }
			end
		end
	end

	def add_deductible_entry
		@deductible = { "name" => "Deductible Name", "value": "0.00" }
		@index = params[:index]

		respond_to do |format|
		    format.html { render :layout => false }
		    format.json { render json: @deductible }
		end
	end

	def add_breakdown
		@customer = params[:cust].present? ? Customer.find(params[:cust]) : Customer.take

		respond_to do |format|
		    format.html 
		    format.json { render json: @deductible }
		end
	end

	def del_breakdown
		@receivable_breakdown = ReceivableBreakdown.find(params[:id])
		@receivable_breakdown.delete

		respond_to do |format|
		    format.html 
		    format.json { render json: { status: 'deleted', data: @receivable_breakdown } }
		end
	end

	def tpl_switcher
		if params[:id].present?
			Receivable.find(params[:id]).update_attribute(template: params[:tpl])
		else
			session[:receivable_tpl] = params[:tpl].present? ? params[:tpl] : (session[:receivable_tpl].present? ? session[:receivable_tpl] : 'default')
		end
	end

	private 
	def fetch_template
		@receivable_tpl = session[:receivable_tpl] = params[:tpl].present? ? params[:tpl] : (session[:receivable_tpl].present? ? session[:receivable_tpl] : 'default')
	end

	def receivable_params
		template_module = "ReceivableTemplates::#{@receivable_tpl.camelize}".constantize

		params.require(:receivable).permit(:customer_id, :cust_ref_number, :gross_sales, :net_sales, :net_amount_due, 
			:template, :process_date, :start_date, :end_date, :customer_address_id, 
			:doc_data => template_module.doc_data_keys, 
			receivable_breakdowns_attributes: [:id, :customer_address_id, :gross_sales, :net_sales, 
				:net_amount_due, :start_date, :end_date, :data => template_module.breakdown_data_keys ])
	end

end
