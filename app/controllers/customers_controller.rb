class CustomersController < ApplicationController
  before_action :authenticate_user!
  before_action :sanitize_params, only: [:create, :update]
  before_action :set_customer, only: [:show, :edit, :update, :destroy, :archive, :save_discount]
  before_action :get_all_brands, only: [:new, :edit, :update]
  before_action :set_salesmen, only: [:new, :edit, :update]

  # GET /customers
  # GET /customers.json
  def index
    @customers = Customer.saved

    respond_to do |format|
      format.html
      format.json {
        render json: CustomerDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
  end

  # GET /customers/new
  def new
    authorize Customer, :allow_add?

    if Customer.unsaved.present?
      @customer = Customer.unsaved.first
    else
      @customer = Customer.new(status: :unsaved)

      @customer.save(validate: false)

      @collection_information = CollectionInformation.new
      @collection_information.customer = @customer
      @collection_information.save(validate: false)

      log_transaction(@customer)
    end
  end

  # GET /customers/1/edit
  def edit
    authorize Customer, :allow_edit?
  end

  # POST /customers
  # POST /customers.json
  def create
    authorize Customer, :allow_add?
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    authorize Customer, :allow_edit?

    @contact_information = ContactInformation.find_by(id: collection_information_params[:contact_info])

    respond_to do |format|
      if @customer.collection_information.present?
        @customer.collection_information.set_contact(@contact_information) if @contact_information.present?
        @customer.collection_information.update(collection_information_params)
      else
        collection_info = CollectionInformation.new(collection_information_params)
        collection_info.customer = @customer
        collection_info.set_contact(@contact_information)
        collection_info.save
      end

      @customer.status = :_new if @customer.unsaved?

      if @customer.update(customer_params)
        log_transaction(@customer)

        format.html { redirect_to @customer, notice: 'Customer was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    authorize Customer, :allow_delete?

    @customer.destroy

    log_transaction(@customer)

    respond_to do |format|
      format.html { redirect_to customers_url, notice: "Customer was successfully #{params[:axn] || 'destroyed'}." }
      format.json { head :no_content }
    end
  end

  def archive
    authorize Customer, :allow_delete?

    respond_to do |format|
      @customer.archived!

      log_transaction(@customer)

      format.html { redirect_to customers_url, notice: "Customer was successfully #{params[:axn] || 'archived'}." }
      format.json { head :no_content }
    end
  end

  def save_discount
    authorize Customer, :allow_edit?

    respond_to do |format|
      format.json do
        discount = Discount.new

        discount.name = params[:name]
        discount.discount = params[:discount]
        discount.discountable = @customer

        if discount.save
          log_transaction(discount)

          render json: { discount: discount.to_json }
        else
          render status: :unprocessable_entity, json: { error_messages: discount.errors.full_messages }
        end
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_customer
    @customer = Customer.find(params[:id])
  end

  def get_all_brands
    @brands = Brand.all
  end

  def set_salesmen
    @salesmen = User.saved.joins(:roles).where(roles: { role_code: 'salesman' })

    respond_to do |format|
      format.html
      format.json { @salesmen }
    end
  end

  def sanitize_params
    sanitize_enum_params(:customer, %w(type_of_receipt classification))
    sanitize_enum_params(:collection_information, %w(credit_term))
    sanitize_formatted_numeric_params(:collection_information, %w(credit_limit), 'to_f')
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def customer_params
    params.require(:customer).permit(:id, :salesman_id, :name, :email, :classification, :owner, :type_of_receipt, :current_balance, :dist_markup, :dist_srp_markup, :admin_credit_hold, :has_bounced_cheq)
  end

  def collection_information_params
    params.require(:collection_information).permit(:counter_day, :counter_time_from, :counter_time_to, :collection_day, :collection_time_from, :collection_time_to, :credit_term, :credit_limit, :contact_info
    )
  end
end
