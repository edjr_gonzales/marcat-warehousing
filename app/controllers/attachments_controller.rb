class AttachmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_attachment

  # DELETE /attachments/:id
  # DELETE /attachments/:id.json
  def destroy
    @attachment.purge

    log_transaction(@attachment, false)

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def set_attachment
    @attachment = ActiveStorage::Attachment.find(params[:id])
  end
end
