class UsersController < Devise::RegistrationsController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_resource_policy, except: [:index, :show, :salesmen]
  skip_before_action :require_no_authentication, only: [:new, :create]

  # GET /users
  # GET /users.json
  def index
    @users = User.saved

    respond_to do |format|
      format.html
      format.json {
        render json: UserDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    authorize @resource, :allow_add?

    if User.unsaved.present?
      @user = User.unsaved.first
    else
      @user = User.new(status: :unsaved, username: 'unsaved')
      @user.save(validate: false)

      @contact_information = ContactInformation.new
      @contact_information.contactable_id = @user.id
      @contact_information.contactable_type = 'User'
      @user.contact_information = @contact_information
    end
  end

  # GET /users/1/edit
  def edit
    authorize @resource, :allow_edit?
  end

  # POST /users
  # POST /users.json
  def create
    authorize @resource, :allow_add?
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    authorize @resource, :allow_edit?

    respond_to do |format|
      @user.status = :_new if @user.unsaved?

      is_update_success = if params[:user][:password].empty?
        @user.update_without_password(user_params)
      else
        @user.update(user_params)
      end

      if is_update_success
        update_permissions
        update_contact
        update_roles

        log_transaction(@user)

        path = if params[:type].empty?
          @user
        else
          user_path(@user, type: params[:type])
        end

        format.html { redirect_to path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit, type: params[:type] }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    authorize @resource, :allow_delete?

    @user.destroy

    log_transaction(@user)

    path = params[:type].present? ? salesmen_users_path : users_url

    respond_to do |format|
      format.html { redirect_to path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def salesmen
    @users = User.salesmen.saved

    respond_to do |format|
      format.html
      format.json {
        render json: SalesmanDatatable.new(params, view_context: view_context)
      }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email, :password, :status, :encrypted_password, :username, :warehouse_id, :is_salesman)
  end

  def set_resource_policy
    @resource = params[:type] == 'salesmen' ? :salesman : User
  end

  def update_roles
    @user.roles.delete_all
    @user.roles << Role.where(role_code: params[:roles].keys) if params[:roles].present?
  end

  def contact_information_params
    params.require(:contact_information).permit(:name, :contact_number_1, :contact_number_2)
  end

  def update_permissions
    @user.permissions.delete_all

    return if is_salesman?

    @user.permissions << Permission.where(permission_code: params[:role_permissions].keys) if params[:role_permissions].present?
  end

  def update_contact
    @user.contact_information.update_attributes(
      name: contact_information_params[:name],
      contact_number_1: contact_information_params[:contact_number_1],
      contact_number_2: contact_information_params[:contact_number_2]
    )
  end

  def is_salesman?
    params[:roles].present? && params[:roles][:salesmen].present?
  end
end
