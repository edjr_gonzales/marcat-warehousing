class DiscountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_discount, only: [:show, :update, :destroy]

  # GET /discounts
  # GET /discounts.json
  def index
    @discounts = Discount.all
  end

  # GET /discounts/1
  # GET /discounts/1.json
  def show
  end

  # GET /discounts/new
  def new
    @discount = Discount.new
  end

  # GET /discounts/1/edit
  def edit
  end

  # POST /discounts
  # POST /discounts.json
  def create
    @discount = Discount.new(discount_params)

    discountable = if params[:resource][:type] == 'Customer'
      Customer.find(params[:resource][:id])
    else
      Supplier.find(params[:resource][:id])
    end

    @discount.discountable = discountable

    respond_to do |format|
      if @discount.save
        log_transaction(@discount)

        format.html { redirect_to @discount, notice: 'Discount was successfully created.' }
        format.json { render :show, status: :created, location: @discount }
      else
        format.html { render :new }
        format.json { render json: @discount.errors, status: :unprocessable_entity }
        logger.debug(@discount.errors.full_messages)
      end
    end
  end

  # PATCH/PUT /discounts/1
  # PATCH/PUT /discounts/1.json
  def update
    respond_to do |format|
      if @discount.update(discount_params)
        log_transaction(@discount)

        format.html { redirect_to @discount, notice: 'Discount was successfully updated.' }
        format.json { render :show, status: :ok, location: @discount }
      else
        format.html { render :edit }
        format.json { render json: @discount.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /discounts/1
  # DELETE /discounts/1.json
  def destroy
    @discount.destroy

    log_transaction(@discount)

    respond_to do |format|
      format.html { redirect_to discounts_url, notice: 'Discount was successfully destroyed.' }
      format.json { render json: { status: 200 } }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_discount
    @discount = Discount.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def discount_params
    params.require(:discount).permit(:id, :brand_id, { discount: [] }, :from_month, :to_month)
  end
end
