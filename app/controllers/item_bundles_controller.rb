class ItemBundlesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_item_bundle, only: [:show, :edit, :update, :destroy]
  before_action :set_items, only: [:new, :create, :edit, :update]

  # GET /item_bundles
  # GET /item_bundles.json
  def index
    @item_bundles = ItemBundle.all

    respond_to do |format|
      format.html
      format.json {
        render json: ItemBundleDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /item_bundles/1
  # GET /item_bundles/1.json
  def show
  end

  # GET /item_bundles/new
  def new
    authorize ItemBundle, :allow_add?

    @item_bundle = ItemBundle.new
  end

  # GET /item_bundles/1/edit
  def edit
    authorize ItemBundle, :allow_edit?
  end

  # POST /item_bundles
  # POST /item_bundles.json
  def create
    authorize ItemBundle, :allow_add?

    @item_bundle = ItemBundle.new(item_bundle_params)

    respond_to do |format|
      items = params[:items]

      if items.uniq.length == items.length
        is_valid = is_breakdown_valid?

        if is_valid
          if @item_bundle.save
            log_transaction(@item_bundle)

            save_breakdown

            format.html { redirect_to @item_bundle, notice: 'Item bundle was successfully created.' }
            format.json { render :show, status: :created, location: @item_bundle }
          else
            format.html { render :new }
            format.json { render json: @item_bundle.errors, status: :unprocessable_entity }
          end
        else
          format.html { render :new }
          format.json { render json: @item_bundle.errors, status: :unprocessable_entity }
        end
      else
        @item_bundle.errors.add(:base, 'There are duplicate items selected')

        format.html { render :new }
        format.json { render json: @item_bundle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /item_bundles/1
  # PATCH/PUT /item_bundles/1.json
  def update
    authorize ItemBundle, :allow_edit?

    respond_to do |format|
      items = params[:items]

      if items.uniq.length == items.length
        is_valid = is_breakdown_valid?

        if is_valid
          if @item_bundle.update(item_bundle_params)
            log_transaction(@item_bundle)

            save_breakdown

            format.html { redirect_to @item_bundle, notice: 'Item bundle was successfully updated.' }
            format.json { render :show, status: :ok, location: @item_bundle }
          else
            format.html { render :edit }
            format.json { render json: @item_bundle.errors, status: :unprocessable_entity }
          end
        else
          format.html { render :new }
          format.json { render json: @item_bundle.errors, status: :unprocessable_entity }
        end
      else
        @item_bundle.errors.add(:base, 'There are duplicate items selected')

        format.html { render :new }
        format.json { render json: @item_bundle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /item_bundles/1
  # DELETE /item_bundles/1.json
  def destroy
    authorize ItemBundle, :allow_delete?

    @item_bundle.destroy

    log_transaction(@item_bundle)

    respond_to do |format|
      format.html { redirect_to item_bundles_url, notice: 'Item bundle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_item_bundle
    @item_bundle = ItemBundle.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_bundle_params
    params.require(:item_bundle).permit(:bundle_set_quantity, :name)
  end

  def set_items
    @items = Item.saved
  end

  def is_breakdown_valid?
    # check all items' inventories if quantities are valid
    is_valid = true
    invalid_item = { name: '', current: 0, requested: 0 }

    params[:items].each_with_index do |id, i|
      next if id.blank?

      item = Item.find(id)
      quantity = params[:item_quantity][i].to_i

      if item.item_inventories.count < quantity.to_i
        is_valid = false
        @item_bundle.errors.add(:base, "Item <strong>#{item.name}</strong> inventories are insufficient. <br /><strong>Current inventory:</strong> #{item.item_inventories.count} <br /><strong>Requested:</strong> #{quantity}")
        break
      end

      total_quantity_for_bundle = quantity.to_i * item_bundle_params[:bundle_set_quantity].to_i

      if item.item_inventories.count < total_quantity_for_bundle
        is_valid = false
        @item_bundle.errors.add(:base, "Item <strong>#{item.name}</strong> inventories are insufficient. <br /><strong>Current Inventory:</strong> #{item.item_inventories.count} <br /><strong>Total requested for the bundle:</strong> #{total_quantity_for_bundle}")
        break
      end
    end
  end

  def save_breakdown
    @item_bundle.bundled_items.destroy_all

    params[:items].each_with_index do |id, i|
      next if id.blank?

      quantity = params[:item_quantity][i]

      bundled_item = BundledItem.new
      bundled_item.item_id = id
      bundled_item.item_bundle = @item_bundle
      bundled_item.quantity = quantity

      bundled_item.save

      log_transaction(bundled_item)
    end
  end
end
