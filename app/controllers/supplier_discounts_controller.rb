class SupplierDiscountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_supplier_discount, only: [:show, :edit, :update, :destroy]
  before_action :set_supplier, only: [:create, :update]

  # GET /supplier_discounts
  # GET /supplier_discounts.json
  def index
    @supplier_discounts = SupplierDiscount.all
  end

  # GET /supplier_discounts/1
  # GET /supplier_discounts/1.json
  def show
  end

  # GET /supplier_discounts/new
  def new
    @supplier_discount = SupplierDiscount.new
  end

  # GET /supplier_discounts/1/edit
  def edit
  end

  # POST /supplier_discounts
  # POST /supplier_discounts.json
  def create
    @supplier_discount = SupplierDiscount.new(supplier_discount_params)

    respond_to do |format|
      @supplier_discount.supplier = @supplier

      if @supplier_discount.save
        format.html { redirect_to @supplier_discount, notice: 'Supplier discount was successfully created.' }
        format.json { render :show, status: :created, location: @supplier_discount }
      else
        format.html { render :new }
        format.json { render json: @supplier_discount.errors, status: :unprocessable_entity }
        logger.debug(@supplier_discount.errors.full_messages)
      end
    end
  end

  # PATCH/PUT /supplier_discounts/1
  # PATCH/PUT /supplier_discounts/1.json
  def update
    respond_to do |format|
      if @supplier_discount.update(supplier_discount_params)
        format.html { redirect_to @supplier_discount, notice: 'Supplier discount was successfully updated.' }
        format.json { render :show, status: :ok, location: @supplier_discount }
      else
        format.html { render :edit }
        format.json { render json: @supplier_discount.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /supplier_discounts/1
  # DELETE /supplier_discounts/1.json
  def destroy
    @supplier_discount.destroy

    respond_to do |format|
      format.html { redirect_to supplier_discounts_url, notice: 'Supplier discount was successfully destroyed.' }
      format.json { render json: { status: 200 } }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_supplier_discount
      @supplier_discount = SupplierDiscount.find(params[:id])
    end

    def set_supplier
      @supplier = Supplier.find(supplier_params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def supplier_discount_params
      params.require(:supplier_discount).permit(:id, :brand_id, { discount: [] }, :from_month, :to_month)
    end

    def supplier_params
      params.require(:supplier).permit(:id)
    end
end
