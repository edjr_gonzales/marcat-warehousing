class CustomerOrdersController < ApplicationController

  before_action :fetch_order
  before_action :set_customer, only: [:index, :new, :show]

	def index
		respond_to do |format|
		    format.html
		    format.json { render json: CustomerOrderDatatable.new(params, view_context: view_context)	 }
		end
	end

	def new
		unsaved = CustomerOrder.where(saved: false, customer: @customer)

		if unsaved.any?
			@customer_order = unsaved.first
		else
			@customer_order = CustomerOrder.create(customer: @customer,
				warehouse: (@customer.user.present? && @customer.user.warehouse.present?) ? @customer.user.warehouse : Warehouse.all.take,
				sales_person: @customer.user.present? ? @customer.user :  current_user,
				billing_address: @customer.customer_addresses.where.not(tax_id: [nil, '']).take,
				shipping_address: @customer.customer_addresses.where(is_business_address: true).take)
		end

		Rails.logger.debug @customer_order.customer.customer_addresses.inspect
	end

	def create; end

	def edit
		respond_to do |format|
			format.html
			format.json { render json: @customer_order }
		end
	end

	def update
		respond_to do |format|
			if @customer_order.update(customer_order_params) && @customer_order.update_attribute(:saved, true)
				format.html { redirect_to customer_customer_orders_path(customer: @customer_order.customer), notice: 'Order was successfully updated.' }
	        	format.json { render :show, status: :ok, location: customer_customer_order_path(customer_order: @customer_order, customer: @customer_order.customer) }
			else
				format.html { render action: :edit }
	        	format.json { render json: @customer_order.errors, status: :unprocessable_entity }
			end
		end
	end

  def show
    respond_to do |format|
      format.html
      format.json { render json: @customer_order }
      format.pdf do
        render pdf: 'naknang_filename',
               layout: 'pdf'
      end
    end
  end

  def destroy
    respond_to do |format|
      if @customer_order.update_attributes(removed: true, removed_at: Time.now)
        format.html { redirect_to action: :index }
        format.json do
          render json: { status: :ok, customer_order: @customer_order }
        end
      else
        format.html do
          redirect_to action: :index, notice: 'Unable to remove Order record.'
        end
        format.json do
          render json: {
            status: :unprocessable_entity,
            receivable: @customer_order
          }
        end
      end
    end
  end

	def add_item_to
		handler = ItemToOrderHandler.execute(params[:item], params[:serial], params[:id], 'add')
		render json: handler.to_json(include: [:order_item, :sales_detail] ), status: handler.result[:status]
	end

  def sub_item_from
    handler = ItemToOrderHandler.execute(params[:item], params[:serial], params[:id], 'sub')
		render json: handler.to_json(include: [:order_item] ), status: handler.result[:status]
  end

  def del_item_from
    handler = ItemToOrderHandler.execute(params[:item], params[:serial], params[:id], 'del')
		render json: handler.to_json(include: [:order_item] ), status: handler.result[:status]
  end

	def items_of
		@customer_order = CustomerOrder.find(params[:id])

		respond_to do |format|
			format.html
			format.json { render json: @customer_order }
			format.js
		end
	end

	private
	def fetch_order
		@customer_order = CustomerOrder.find(params[:id]) if params[:id].present?
	end

  def customer_order_params
    params.require(:customer_order).permit(
      :customer_id,
      :warehouse_id,
      :gross_amount,
      :net_amount,
      :net_amount_due,
      :delivery_date,
      :sale_date,
      :shipping_address_id,
      :billing_address_id,
      :checker_ids => []
    )
  end

  def set_customer
    @customer = Customer.find(params[:customer_id])
  end
end
