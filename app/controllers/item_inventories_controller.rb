class ItemInventoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_item
  before_action :set_item_inventory, only: [:show, :edit, :update, :destroy]

  # GET /items/:id/item_inventories
  # GET /items/:id/item_inventories.json
  def index
    @item_inventories = @item.item_inventories

    respond_to do |format|
      format.html
      format.json {
        render json: ItemInventoryDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /items/:id/item_inventories/serials
  # GET /items/:id/item_inventories/serials.json
  def serials
    @serials = @item.item_inventories.sold.pluck(:serial_id).compact

    respond_to do |format|
      format.json { render json: { serials: @serials } }
    end
  end

  # GET /items/:id/item_inventories/1
  # GET /items/:id/item_inventories/1.json
  def show
  end

  # GET /items/:id/item_inventories/new
  def new
    @item_inventory = @item.item_inventories.new
  end

  # GET /items/:id/item_inventories/1/edit
  def edit
  end

  # POST /items/:id/item_inventories
  # POST /items/:id/item_inventories.json
  def create
    item_inventories = []

    case params[:by]
      when 'bulk'
        quantity = params[:quantity].to_i

        quantity.times do
          item_inventory = build_item_inventory

          log_transaction(item_inventory)

          item_inventories << as_json(item_inventory)
        end
      when 'serial'
        item_inventory = build_item_inventory(item_inventory_params[:serial_id])

        log_transaction(item_inventory)

        item_inventories << as_json(item_inventory)
    end

    respond_to do |format|
      format.json { render json: item_inventories, status: 200 }
    end
  end

  # PATCH/PUT /items/:id/item_inventories/1
  # PATCH/PUT /items/:id/item_inventories/1.json
  def update
    respond_to do |format|
      @item_inventory.stock_section_id = params[:stock_section_id]

      if @item_inventory.update(item_inventory_params)
        log_transaction(@item_inventory)

        format.html { redirect_to @item_inventory, notice: 'Item inventory was successfully updated.' }
        format.json { render json: as_json(@item_inventory), status: 200 }
      else
        format.html { render :edit }
        format.json { render json: @item_inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/:id/item_inventories/1
  # DELETE /items/:id/item_inventories/1.json
  def destroy
    @item_inventory.destroy

    log_transaction(@item_inventory)

    respond_to do |format|
      format.html { redirect_to item_inventories_url, notice: 'Item inventory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # DELETE /items/:id/item_inventories/delete_by_bulk
  # DELETE /items/:id/item_inventories/delete_by_bulk.json
  def delete_by_bulk
    bulk = ItemInventory.bulk(params[:item_id], current_user.id, params[:quantity])

    bulk.destroy_all

    bulk.each do |item|
      log_transaction(item)
    end

    respond_to do |format|
      format.json { render json: bulk.as_json, status: 200 }
    end
  end

  # GET /items/:id/item_inventories/on_stock
  def on_stock
    respond_to do |format|
      format.json { render json: { on_stock: @item.item_inventories.stock.count }, status: 200 }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_item_inventory
    @item_inventory = @item.item_inventories.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_inventory_params
    params.require(:item_inventory).permit(:serial_id)
  end

  def set_item
    @item = Item.find(params[:item_id])
  end

  def build_item_inventory(serial = nil)
    item_inventory = ItemInventory.new
    item_inventory.item = @item
    item_inventory.user = current_user
    item_inventory.item_status = :stock
    item_inventory.stock_section_id = params[:stock_section_id]
    item_inventory.serial_id = serial if serial

    item_inventory.save

    item_inventory
  end

  def as_json(item_inventory)
    item_inventory.as_json(include: [{ user: { only: [:username] }, stock_section: { only: [:name] } }])
  end
end
