class WarehousesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_warehouse, only: [:show, :edit, :update, :destroy]

  # GET /warehouses
  # GET /warehouses.json
  def index
    @warehouses = Warehouse.saved

    respond_to do |format|
      format.html
      format.json {
        render json: WarehouseDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /warehouses/1
  # GET /warehouses/1.json
  def show
  end

  # GET /warehouses/new
  def new
    authorize Warehouse, :allow_add?

    if Warehouse.unsaved.present?
      @warehouse = Warehouse.unsaved.first
    else
      @warehouse = Warehouse.new(status: :unsaved)

      @warehouse.save(validate: false)

      log_transaction(@warehouse)
    end
  end

  # GET /warehouses/1/edit
  def edit
    authorize Warehouse, :allow_edit?
  end

  # POST /warehouses
  # POST /warehouses.json
  def create
    authorize Warehouse, :allow_add?
  end

  # PATCH/PUT /warehouses/1
  # PATCH/PUT /warehouses/1.json
  def update
    authorize Warehouse, :allow_edit?

    respond_to do |format|
      @warehouse.status = :_new if @warehouse.unsaved?

      @warehouse.personnels.delete_all

      if params[:personnels]
        params[:personnels].each do |id|
          @warehouse.personnels << User.find(id)
        end
      end

      if @warehouse.update(warehouse_params)
        log_transaction(@warehouse)

        format.html { redirect_to @warehouse, notice: 'Warehouse was successfully updated.' }
        format.json { render :show, status: :ok, location: @warehouse }
      else
        format.html { render :edit }
        format.json { render json: @warehouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /warehouses/1
  # DELETE /warehouses/1.json
  def destroy
    authorize Warehouse, :allow_delete?

    @warehouse.destroy

    log_transaction(@warehouse)

    respond_to do |format|
      format.html { redirect_to warehouses_url, notice: "Warehouse was successfully #{params[:axn] || 'destroyed'}." }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_warehouse
    @warehouse = Warehouse.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def warehouse_params
    params.require(:warehouse).permit(:id, :name, :wh_code, :wh_address)
  end
end
