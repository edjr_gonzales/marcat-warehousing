class AddressesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_address, only: [:show, :edit, :update, :destroy, :make_primary]
  before_action :set_resource, only: [:index, :create, :update, :make_primary]

  # GET /addresses
  # GET /addresses.json
  def index
    @addresses = @resource.addresses
  end

  # GET /addresses/1
  # GET /addresses/1.json
  def show; end

  # GET /addresses/new
  def new
    @address = Address.new
  end

  # GET /addresses/1/edit
  def edit; end

  # POST /addresses
  # POST /addresses.json
  def create
    @address = Address.new(address_params)

    if resource_params[:type].eql?('Customer')
      @resource_address = CustomerAddress.new(addressable_params)
      @contact_information = ContactInformation.find_by(id: addressable_params[:contact_info])
      @resource_address.customer = @resource
    else
      @resource_address = SupplierAddress.new(addressable_params)
      @contact_information = ContactInformation.find_by(id: addressable_params[:contact_info])
      @resource_address.supplier = @resource
    end

    respond_to do |format|
      @resource_address.set_contact(@contact_information) if @contact_information.present?

      @address.addressable = @resource_address if @resource_address.save

      if @address.save
        log_transaction(@address)

        format.html { redirect_to @address, notice: 'Address was successfully created.' }
        format.json { render json: { status: :ok, address: @address, addressable: @resource_address } }
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /addresses/1
  # PATCH/PUT /addresses/1.json
  def update
    respond_to do |format|
      @resource_address = @address.addressable
      @contact_information = ContactInformation.find_by(id: addressable_params[:contact_info])

      if @resource_address.present?
        @resource_address.set_contact(@contact_information) if @contact_information.present?
        @resource_address.update(addressable_params)
      else
        if resource_params[:type].eql?('Customer')
          @resource_address = CustomerAddress.new(addressable_params)
          @resource_address.customer = @resource
        else
          @resource_address = SupplierAddress.new(addressable_params)
          @resource_address.supplier = @resource
        end

        @resource_address.set_contact(@contact_information)

        @address.addressable = @resource_address if @resource_address.save
      end

      if @address.update(address_params)
        log_transaction(@address)

        format.html { redirect_to @address, notice: 'Address was successfully updated.' }
        format.json { render json: { status: :ok, address: @address, addressable: @resource_address } }
      else
        format.html { render :edit }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /addresses/1
  # DELETE /addresses/1.json
  def destroy
    @address.destroy

    log_transaction(@address)

    respond_to do |format|
      format.html { redirect_to addresss_url, notice: 'Address was successfully destroyed.' }
      format.json { render json: { status: 200 } }
    end
  end

  # PATCH /addresses/1/make_primary
  # PATCH /addresses/1/make_primary.json
  def make_primary
    previous_primary = @resource.customer_addresses
                                .where(is_primary: true)
                                .first

    @resource.customer_addresses.update_all(is_primary: false)
    @address.addressable.update(is_primary: true)

    data = {
      status: :ok,
      address: @address,
      addressable: @address.addressable
    }

    if previous_primary.present?
      data[:previous_primary] = {
        previous_primary: {
          address: previous_primary.addresses.first,
          addressable: previous_primary
        }
      }
    end

    log_transaction(@address)

    respond_to do |format|
      format.json { render json: data }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_address
    @address = Address.find(params[:id])
  end

  def set_resource
    @resource = if resource_params[:type].eql?('Customer')
                  Customer.find(resource_params[:id])
                else
                  Supplier.find(resource_params[:id])
                end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def address_params
    params.require(:address).permit(
      :id,
      :basic_address,
      :town_city,
      :state_province,
      :country
    )
  end

  def addressable_params
    params.require(:addressable).permit(
      :is_primary,
      :is_business_address,
      :is_home_address,
      :is_delivery_address,
      :fax, :delivery_schedule_day,
      :delivery_schedule_time,
      :business_name,
      :tax_id,
      :contact_info
    )
  end

  def resource_params
    params.require(:resource).permit(:id, :type)
  end
end
