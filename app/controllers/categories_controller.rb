class CategoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.saved.main

    respond_to do |format|
      format.html
      format.json {
        render json: CategoryDatatable.new(params, view_context: view_context)
      }
    end
  end

  # GET /categories/1/sub_categories
  # GET /categories/1/sub_categories.json
  def sub_categories
    @category = Category.find(params[:category_id])
    @categories = @category.sub_categories

    respond_to do |format|
      format.html
      format.json { render json: @categories.as_json }
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
  end

  # GET /categories/new
  def new
    authorize Category, :allow_add?

    if Category.unsaved.present?
      @category = Category.unsaved.first
    else
      @category = Category.new(status: :unsaved)

      @category.save(validate: false)

      log_transaction(@category)
    end
  end

  # GET /categories/1/edit
  def edit
    authorize Category, :allow_edit?

  end

  # POST /categories
  # POST /categories.json
  def create
    authorize Category, :allow_add?

    parent_category = Category.find(params[:category][:id])

    @category = Category.new(sub_category_params)
    @category.parent_category = parent_category
    @category.status = :_new

    respond_to do |format|
      if @category.save
        log_transaction(@category)

        format.html { redirect_to @category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    authorize Category, :allow_edit?

    paramz = @category.is_main? ?  category_params : sub_category_params

    respond_to do |format|
      @category.status = :_new if @category.unsaved?

      if @category.update(paramz)
        log_transaction(@category)

        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    authorize Category, :allow_delete?

    @category.destroy

    log_transaction(@category)

    respond_to do |format|
      format.html { redirect_to categories_url, notice: "Category was successfully #{params[:axn] || 'destroyed'}." }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_category
    id = params[:sub_category].present? ? params[:sub_category][:id] : params[:id]

    @category = Category.find_by(id: id)
    @category = Category.find(params[:id]) if @category.nil?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def category_params
    params.require(:category).permit(:name, :short_code)
  end

  def sub_category_params
    params.require(:sub_category).permit(:name, :short_code)
  end
end
