class ItemInventoryDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def initialize(params, opts = {})
    @view = opts[:view_context]
    @item = Item.find(params[:item_id])
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      username: { source: 'User.username' },
      name: { source: 'StockSection.name' },
      item_status: { source: 'ItemInventory.item_status' },
      serial_id: { source: 'ItemInventory.serial_id' },
      actions: { source: 'ItemInventory.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        username: record.user.username,
        name: record.stock_section.name,
        item_status: record.item_status.titleize,
        serial_id: record.serial_id || 'N/A',
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    @item.item_inventories.joins([:stock_section, :user])
  end
end
