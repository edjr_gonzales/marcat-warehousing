class BrandDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      name: { source: 'Brand.name' },
      short_code: { source: 'Brand.short_code' },
      actions: { source: 'Brand.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        name: record.name,
        short_code: record.short_code,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    Brand.all
  end
end
