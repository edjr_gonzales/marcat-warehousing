class ItemBundleDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      name: { source: 'ItemBundle.name' },
      bundle_set_quantity: { source: 'ItemBundle.bundle_set_quantity' },
      items: { source: 'Item.name' },
      actions: { source: 'ItemBundle.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        name: record.name,
        bundle_set_quantity: record.bundle_set_quantity,
        items: record.items.pluck(:name).join(', '),
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    ItemBundle.joins(:items)
  end
end
