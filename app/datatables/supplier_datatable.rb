class SupplierDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def_delegator :@view, :check_box_tag

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      checkbox: { source: 'Supplier.id' },
      name: { source: 'Supplier.name' },
      email: { source: 'Supplier.email' },
      tax_id: { source: 'Supplier.tax_id' },
      credit_term: { source: 'AccountingInformation.credit_term' },
      current_balance: { source: 'Supplier.current_balance' },
      status: { source: 'Supplier.status' },
      actions: { source: 'Supplier.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        checkbox: check_box_tag('ids[]', record.id, false, class: 'ids'),
        name: record.name,
        email: record.email,
        tax_id: record.tax_id,
        credit_term: record.accounting_information.credit_term.try(:titleize) || 'N/A',
        current_balance: record.current_balance,
        status: record.status.titleize,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    status = 'saved'
    status = params[:supplier_status] if params[:supplier_status].present?

    Supplier.joins([:accounting_information]).send(status)
  end
end
