class ReturnSlipDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      sku: { source: 'Item.sku' },
      item_inventory_serial: { source: 'ReturnSlip.item_inventory_serial' },
      description: { source: 'ReturnSlip.description' },
      reasons: { source: 'ReturnSlip.reasons' },
      total_quantity: { source: 'ReturnSlip.total_quantity' },
      unit_price: { source: 'ReturnSlip.unit_price' },
      price: { source: 'ReturnSlip.price' },
      actions: { source: 'ReturnSlip.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        sku: record.item.try(:sku) || 'N/A',
        item_inventory_serial: record.item_inventory_serial || 'N/A',
        description: record.description,
        reasons: record.reasons.join(', '),
        total_quantity: record.total_quantity,
        unit_price: record.unit_price,
        price: record.price,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    ReturnSlip.joins([:item])
  end
end
