class CustomerDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def_delegator :@view, :check_box_tag

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      checkbox: { source: 'Customer.id' },
      name: { source: 'Customer.name' },
      current_balance: { source: 'Customer.current_balance' },
      status: { source: 'Customer.status' },
      actions: { source: 'Customer.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        checkbox: check_box_tag('ids[]', record.id, false, class: 'ids'),
        name: record.name,
        current_balance: record.current_balance,
        status: record.status.titleize,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    status = 'saved'
    status = params[:customer_status] if params[:customer_status].present?

    Customer.send(status)
  end
end
