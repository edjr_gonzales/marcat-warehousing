class RoleDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      name: { source: 'Role.name' },
      role_code: { source: 'Role.role_code' },
      description: { source: 'Role.description' },
      actions: { source: 'Role.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        name: record.name,
        role_code: record.role_code,
        description: record.description,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    Role.all
  end
end
