class WarehouseDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      name: { source: 'Warehouse.name' },
      wh_code: { source: 'Warehouse.wh_code' },
      wh_address: { source: 'Warehouse.wh_address' },
      actions: { source: 'Warehouse.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        name: record.name,
        wh_code: record.wh_code,
        wh_address: record.wh_address,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    Warehouse.saved
  end
end
