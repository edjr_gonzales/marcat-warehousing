class ReceivableDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def_delegators :@view, :link_to, :check_box_tag, :customer_path, :receivable_path

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "Receivable.id" },
      reference_no: { source: "Receivable.sys_ref_number", cond: :eq },
      customer_name: { source: "Customer.name", cond: :like },
      gross_sales: { source: "Receivable.gross_sales_cents" },
      net_sales: { source: "Receivable.net_sales_cents" },
      net_amount_due: { source: "Receivable.net_amount_due_cents" },
      sale_date_start: { source: "Receivable.start_date" },
      sale_date_end: { source: "Receivable.end_date" },
      processing_date: { source: "Receivable.process_date" }
    }
  end

  def data
    records.map do |record|
      {
        id:               check_box_tag('receivables[]', record.id),
        reference_no:     link_to(record.sys_ref_number, receivable_path(record)),
        customer_name:    link_to(record.customer.name, customer_path(record.customer)),
        gross_sales:      record.gross_sales.format,
        net_sales:        record.net_sales.format,
        net_amount_due:   record.net_amount_due.format,
        sale_date_start:  record.start_date.strftime("%m-%d-%Y"),
        sale_date_end:    record.end_date.strftime("%m-%d-%Y"),
        processing_date:  record.process_date.strftime("%m-%d-%Y")
      }
    end
  end

  def get_raw_records
    Receivable.joins(:customer) #.order("receivables.updated_at DESC")
  end

end
