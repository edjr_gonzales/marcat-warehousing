class CustomerOrderDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def_delegators :@view, :link_to, :check_box_tag, :edit_customer_customer_order_path

  def initialize(params, opts = {})
    @view = opts[:view_context]
    @customer_id = params[:customer_id]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      reference_no: { source: "CustomerOrder.trans_ref_number", cond: :like, searchable: true, orderable: true },
      sale_date: { source: "CustomerOrder.sale_date", cond: :like, searchable: true, orderable: true },
      gross_sales: { source: "CustomerOrder.gross_sales_cents", orderable: false },
      net_sales: { source: "CustomerOrder.net_sales_cents", orderable: false },
      net_amount_due: { source: "CustomerOrder.net_amount_due_cents", orderable: false },
      warehouse: { source: "Warehouse.name", orderable: false },
      salesman: { source: "User.username", orderable: true },
      delivery_date: { source: "CustomerOrder.delivery_date", cond: :like, searchable: true, orderable: true }
    }
  end

  def data
    records.map do |record|
      {
        reference_no:     link_to(record.trans_ref_number, edit_customer_customer_order_path(customer_id: @customer_id, id: record.id)),
        sale_date:        record.sale_date.strftime("%Y-%m-%d"),
        gross_sales:     record.gross_sales.format,
        net_sales:        record.net_sales.format,
        net_amount_due:   record.net_amount_due.format,
        warehouse:        record.warehouse.name,
        salesman:         record.sales_person.username,
        delivery_date:    record.delivery_date.strftime("%Y-%m-%d")
      }
    end
  end

  def get_raw_records
    CustomerOrder.where(customer_id: @customer_id, saved: true, removed: false).joins(:customer, :warehouse, :sales_person)
  end
end
