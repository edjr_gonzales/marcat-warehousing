class ItemDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def_delegator :@view, :get_brand_name

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      name: { source: 'Item.name' },
      brand: { source: 'Brand.name' },
      item_code: { source: 'Item.sku' },
      price: { source: 'Item.price' },
      dealer_price: { source: 'Item.dealer_price' },
      srp_price: { source: 'Item.srp_price' },
      actions: { source: 'Item.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        name: record.name,
        brand: get_brand_name(record.brand.id),
        item_code: record.sku,
        price: record.price,
        dealer_price: record.dealer_price,
        srp_price: record.srp_price,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    Item.joins([:brand]).saved
  end
end
