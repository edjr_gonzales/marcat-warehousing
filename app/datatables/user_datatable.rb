class UserDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      name: { source: 'ContactInformation.name' },
      username: { source: 'User.username' },
      email: { source: 'User.email' },
      actions: { source: 'User.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        name: record.contact_information.name,
        username: record.username,
        email: record.email,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    User.joins([:contact_information]).saved
  end
end
