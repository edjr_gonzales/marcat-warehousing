class OrderSlipDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable

  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      customer: { source: 'Customer.name' },
      warehouse: { source: 'Warehouse.name' },
      status: { source: 'OrderSlip.status' },
      actions: { source: 'OrderSlip.id' }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        customer: record.orderable.name,
        warehouse: record.warehouse.name,
        status: record.status.humanize,
        actions: record.decorate.dt_actions(record)
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    OrderSlip.joins([:warehouse])
  end
end
