class SalesmanPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def allow_add?
    user.is_root_admin? || user.has_permission?('salesmen_allow_add')
  end

  def allow_edit?
    user.is_root_admin? || user.has_permission?('salesmen_allow_edit')
  end

  def allow_delete?
    user.is_root_admin? || user.has_permission?('salesmen_allow_delete')
  end

  def allow_sell?
    user.is_root_admin? || user.has_permission?('salesmen_allow_sell')
  end
end
