class SubCategoryPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def allow_add?
    user.is_root_admin? || user.has_permission?('sub_categories_allow_add')
  end

  def allow_edit?
    user.is_root_admin? || user.has_permission?('sub_categories_allow_edit')
  end

  def allow_delete?
    user.is_root_admin? || user.has_permission?('sub_categories_allow_delete')
  end
end
