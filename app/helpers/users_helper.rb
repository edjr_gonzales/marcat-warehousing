module UsersHelper
  def available_role_permissions
    Role.includes([:permissions]).where.not(role_code: 'root').order('permissions.name ASC')
  end
end
