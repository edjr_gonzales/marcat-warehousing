module CustomersHelper
	def get_salesman(id)
		if id.present?
			salesman = ContactInformation.find(id)
			return salesman.name
		end
	end
end
