module ItemsHelper
  def add_item_inventories_btn_data
    by = @item.is_identifiable ? 'serial' : 'bulk'
    target = "#item-inventory-by-#{by}-form-modal"

    return { by: by, target: target }
  end
end
