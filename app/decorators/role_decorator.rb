class RoleDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def dt_actions(record)
    links = []

    links << h.link_to('Show', record, class: 'action-item btn btn-sm btn-outline-info')
    links << h.link_to('Edit', h.edit_role_path(record), class: 'action-item btn btn-sm btn-outline-warning')
    links << h.link_to('Destroy', record, method: :delete, class: 'action-item btn btn-sm btn-outline-danger', data: { confirm: 'Are you sure?' })

    h.safe_join(links)
  end
end
