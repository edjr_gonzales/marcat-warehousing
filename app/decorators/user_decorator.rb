class UserDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  # Just an example of a complex method you can add to you decorator
  # To render it in a datatable just add a column 'dt_actions' in
  # 'view_columns' and 'data' methods and call record.decorate.dt_actions
  def dt_actions(record)
    links = []

    links << h.link_to('Show', record, class: 'action-item btn btn-sm btn-outline-info')
    links << h.link_to('Edit', h.edit_user_path(record), class: 'action-item btn btn-sm btn-outline-warning') if h.policy(User).allow_edit?
    links << h.link_to('Destroy', record, method: :delete, class: 'action-item btn btn-sm btn-outline-danger', data: { confirm: 'Are you sure?' }) if h.policy(User).allow_delete?

    h.safe_join(links)
  end
end
