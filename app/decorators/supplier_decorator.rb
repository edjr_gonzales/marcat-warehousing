class SupplierDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def dt_actions(record)
    links = []

    links << h.link_to('Show', record, class: 'action-item btn btn-sm btn-outline-info')
    links << h.link_to('Edit', h.edit_supplier_path(record), class: 'action-item btn btn-sm btn-outline-warning') if h.policy(Supplier).allow_edit?

    if h.policy(Supplier).allow_delete?
      links << if record.archived?
        h.link_to 'Delete Supplier Data', record, method: :delete, class: 'btn btn-sm btn-outline-danger', data: { confirm: 'Are you sure?' }
      else
        h.link_to 'Archive Supplier Data', h.archive_supplier_path(record), class: 'btn btn-sm btn-outline-primary', data: { confirm: 'Are you sure?' }
      end
    end

    h.safe_join(links)
  end
end
