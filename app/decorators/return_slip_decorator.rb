class ReturnSlipDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def dt_actions(record)
    links = []

    links << h.link_to('Show', return_slip, class: 'btn btn-sm btn-outline-info')

    h.safe_join(links)
  end
end
