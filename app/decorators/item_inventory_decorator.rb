class ItemInventoryDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def dt_actions(record)
    links = []

    if (record.serial_id && record.stock?) && h.policy(ItemInventory).allow_edit?
      links << h.link_to('#', class: 'edit-item-inventory', data: { id: record.id, by: record.serial_id ? 'serial' : 'bulk' }) do
      	h.content_tag :i, class: 'fa fa-edit' do
      		''
      	end
      end
    end

    if record.stock? && h.policy(ItemInventory).allow_edit?
      links << h.link_to('#', class: 'destroy-item-inventory', data: { id: record.id }) do
        h.content_tag :i, class: 'fa fa-trash' do
        	''
        end
      end
    end

    h.safe_join(links)
  end
end
