jQuery(document).ready(function() {
  jQuery('#customer-order-list').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": $('#customer-order-list').data('source')
    },
    "pagingType": "full_numbers",
    "columns": [
      {"data": "reference_no"},
      {"data": "sale_date"},
      {"data": "gross_sales"},
      {"data": "net_sales"},
      {"data": "net_amount_due"},
      {"data": "warehouse"},
      {"data": "salesman"},
      {"data": "delivery_date"}
    ]
  });
});
