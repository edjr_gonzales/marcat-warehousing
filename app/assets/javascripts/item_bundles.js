$(function() {
  $('#item_ids').select2();

  var indeces = [];

  $('[data-index]').each(function(e) {
    indeces.push(parseInt($(this).data('index')));
  });

  var count = Math.max(...indeces) + 1;

  $('.add-bundled-item-btn').off('click').on('click', function(e) {
    e.preventDefault();

    var itemName =  '<div class=\'col-8\' id=\'item-name-container-'+ count +'\'>';
    itemName +=       '<div class=\'form-group\'>';
    itemName +=         '<label for=\'item-name-'+ count +'\'>Item Name</label>';
    itemName +=         '<select name=\'items[]\' class=\'form-control\' data-index=\''+ count +'\' id=\'item-name-'+ count +'\'>';
    itemName +=           '<option value=\'\'>Select an Item...</option>';
    itemName +=           setItemsOptions();
    itemName +=         '</select>';
    itemName +=       '</div>';
    itemName +=     '</div>';

    var itemQuantity =  '<div class=\'col-4\' id=\'item-quantity-container-'+ count +'\'>';
    itemQuantity +=       '<div class=\'form-group\'>';
    itemQuantity +=         '<label for=\'item-quantity-'+ count +'\'>Item Quantity</label>';
    itemQuantity +=         '<a href=\'#\' class=\'remove-item-section\' data-index=\''+ count +'\'><i class=\'fa fa-times\'></i></a>'
    itemQuantity +=         '<input type=\'text\' class=\'form-control\' name=\'item_quantity[]\' id=\'item-quantity-'+ count +'\'/>';
    itemQuantity +=       '</div>';
    itemQuantity +=     '</div>';

    $('#items').append(itemName);
    $('#items').append(itemQuantity);

    reloadRemoveItemSection();

    count += 1;
  });

  reloadRemoveItemSection();
});

function setItemsOptions() {
  var options = '';

  for(item of items) {
    options += '<option value=\''+ item.id +'\'>'+ item.name +'</option>';
  }

  return options;
}

function reloadRemoveItemSection() {
  $('.remove-item-section').off('click').on('click', function(e) {
    e.preventDefault();

    const index = $(this).data('index');

    $('#item-name-container-'+ index).remove();
    $('#item-quantity-container-'+ index).remove();
  })
}
