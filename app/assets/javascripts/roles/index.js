$(function(){
	$('#roles-list').dataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#roles-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'name' },
      { data: 'role_code' },
      { data: 'description' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [3]
    	}
    ]
	});
});
