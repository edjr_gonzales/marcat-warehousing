jQuery(document).ready(function() {
  jQuery('#receivables-list').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": $('#receivables-list').data('source')
    },
    "pagingType": "full_numbers",
    "columns": [
      {"data": "id"},
      {"data": "reference_no"},
      {"data": "customer_name"},
      {"data": "gross_sales"},
      {"data": "net_sales"},
      {"data": "net_amount_due"},
      {"data": "sale_date_start"},
      {"data": "sale_date_end"},
      {"data": "processing_date"}
    ]
  });
});