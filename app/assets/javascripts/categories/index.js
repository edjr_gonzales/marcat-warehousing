$(function(){
	$('#categories-list').dataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#categories-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'name' },
      { data: 'short_code' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [2]
    	}
    ]
	});
});
