$(function(){
	$('#warehouses-list').dataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#warehouses-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'name' },
      { data: 'wh_code' },
      { data: 'wh_address' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [3]
    	}
    ]
	});
});
