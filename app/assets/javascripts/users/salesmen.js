$(function() {
	$('#salesmen-list').dataTable({
		processing: true,
		serverSide: true,
		ajax: {
			url: $('#salesmen-list').data('source')
		},
		pagingType: 'full_numbers',
		columns: [
			{ data: 'name' },
			{ data: 'username' },
			{ data: 'email' },
			{ data: 'actions' }
		],
		aoColumnDefs: [
			{
				bSortable: false,
				aTargets: [3]
			}
		]
	});
});
