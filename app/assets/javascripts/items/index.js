$(function(){
	$('#items-list').dataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#items-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'name' },
      { data: 'brand' },
      { data: 'item_code' },
      { data: 'price' },
      { data: 'dealer_price' },
      { data: 'srp_price' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [6]
    	}
    ]
	});
});
