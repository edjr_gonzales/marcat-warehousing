$(function(){
  $('#item_ids').off('change').on('change', function(e) {
    var id = $(this).val();

    $.ajax({
      url: '/items/'+ id +'/item_inventories/serials',
      dataType: 'json',
      success: function(data) {
        const serials = data.serials;

        if(serials.length <= 0) return;

        $('#item_inventory_serials').empty();

        var options = '<option value="">Select Serial No...</option>';

        serials.forEach(function(serial) {
          options += '<option value=\''+ serial +'\'>'+ serial +'</option>';
        })

        $('#item_inventory_serials').append(options);
      }
    });
  });

  $('#item_inventory_serials').off('blur').on('blur', function(e) {
    if($(this).val().length) {
      var $totalItems = $('[name=\'return_slip[total_quantity]\']');
      $totalItems.val('1');
      $totalItems.attr('readonly', 'readonly');
    }
  });
});
