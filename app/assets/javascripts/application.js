// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require sb-admin/sb-admin.min
//= require popper
//= require bootstrap
//= require bootstrap_sb_admin_base_v1
//= require rails-ujs
//= require datatables
//= require jquery-ui
//= require lodash
//= require plugins/jquery.datetimepicker.full.min
//= require plugins/jquery.weekline.min
//= require plugins/jquery.inputfilter.min
//= require plugins/select2.min
//= require plugins/bootstrap-select.min
//= require owl.carousel

// require turbolinks
// require_tree .

$(document).ready(function(){
  jQuery('.date-picker-field').datetimepicker({
    format:'Y-m-d',
    timepicker:false
  });
});
