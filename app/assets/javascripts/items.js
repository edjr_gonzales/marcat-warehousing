var $row = null;
var itemId = $('[name=\'items[id]\']').val();

var fields = {
  'item_inventory': ['id', 'serial_id'],
  'warehouse': ['id'],
  'stock_section': ['id']
}

$(function() {
  $('#sub_category_ids').select2();
  $('#item-inventories-list').DataTable({
    processing: true,
    serverSide: true,
    ajax: {
      url: $('#item-inventories-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'username' },
      { data: 'name' },
      { data: 'item_status' },
      { data: 'serial_id' },
      { data: 'actions' },
    ],
    aoColumnDefs: [
      {
        bSortable: false,
        aTargets: [4]
      }
    ],
    'drawCallback': function(settings) {
      reloadDeleteItemInventoryEvent();
      reloadEditItemInventoryEvent();
    }
  });

  reloadDeleteByBulkEvent();
  reloadOwlCarouselManager();
  reloadDeleteAttachmentEvent();

  $('.save-item-inventory').off('click').on('click', function(e) {
    e.preventDefault();

    var warehouseId, stockSectionId, quantity;

    $('.warehouse_ids').each(function(e) {
      if($(this).val()) {
        warehouseId = $(this).val();
        return;
      }
    })

    $('.stock_section_ids').each(function(e) {
      if($(this).val()) {
        stockSectionId = $(this).val();
        return;
      }
    })

    var type = $(this).data('by');
    var serialId = $('[name=\'item_inventory[serial_id]\']').val();

    var params = {
      warehouse_id: warehouseId,
      stock_section_id: stockSectionId,
      by: type
    };

    if(type === 'serial') {
      params.item_inventory = { serial_id: serialId };
    } else {
      $('.quantity').each(function(e) {
        if($(this).val()) {
          quantity = $(this).val();
          return;
        }
      });

      params.quantity = quantity;
    }

    if($('[name=\'item_inventory[id]\']').val()) {
      params['_method'] = 'put';
      var itemInventoryId = $('[name=\'item_inventory[id]\']').val();

      $.ajax({
        url: '/items/'+ itemId +'/item_inventories/'+ itemInventoryId +'.json',
        method: 'POST',
        dataType: 'json',
        data: params,
        success: function(data) {
          $('#item-inventory-by-bulk-form-modal').modal('hide');
          $('#item-inventory-by-serial-form-modal').modal('hide');

          $('#item-inventories-list').DataTable().row($row).data(getItemInventoryData(data)).draw(false);
        }
      })
    } else {
      $.ajax({
        url: '/items/'+ itemId +'/item_inventories.json',
        dataType: 'json',
        method: 'post',
        data: params,
        success: function(data) {
          $('#item-inventory-by-bulk-form-modal').modal('hide');
          $('#item-inventory-by-serial-form-modal').modal('hide');

          var count = 0;

          data.forEach(function(itemInventoryData) {
            console.log({ naknang: getItemInventoryData(itemInventoryData) });

            $('#item-inventories-list').DataTable().row.add(getItemInventoryData(itemInventoryData)).draw(false);

            count += 1;
          });

          ['on-stock', 'total'].forEach(function(sec) {
            $('.'+ sec).text(parseInt($('.'+ sec).text()) + count);
          });
        }
      })
    }
  });

  $('.warehouse_ids').off('change').on('change', function(e) {
    var id = $(this).val();

    $.ajax({
      url: '/warehouses/'+ id +'/stock_sections',
      dataType: 'json',
      success: function(data) {
        $('.stock_section_ids').empty();

        var options = '<option value="">Select a stock section...</option>';

        data.forEach(function(stockSection) {
          options += '<option value=\''+ stockSection.id +'\'>'+ stockSection.name +'</option>';
        });

        $('.stock_section_ids').append(options);
        $('.edit-item-inventory').trigger('stockSectionIdPopulated');
      }
    })
  });

  $('#category_id').off('change').on('change', function(e) {
    var id = $(this).val();

    if(!id.length) return;

    $.ajax({
      url: '/categories/'+ id +'/sub_categories.json',
      dataType: 'json',
      success: function(data) {
        $('#sub_category_ids').empty();

        options = '';

        data.forEach(function(subCategory) {
          options += '<option value=\''+ subCategory.id +'\'>'+ subCategory.name +'</option>';
        });

        $('#sub_category_ids').append(options);
        $('#sub_category_ids').trigger('change');
      }
    })
  })
});

var reloadDeleteAttachmentEvent = function() {
  $('.delete-attachment-btn').off('click').on('click', function(e) {
    e.preventDefault();

    AppHelper.loader('toggle');

    let index = parseInt($(this).attr('data-index'));

    if(confirm('Are you sure?')) {
      $.ajax({
        url: '/attachments/'+ $(this).attr('data-id') +'.json',
        method: 'DELETE',
        dataType: 'json',
        success: function(data) {
          AppHelper.loader('toggle');

          $(".owl-carousel").trigger('remove.owl.carousel', index).trigger('refresh.owl.carousel');
          $('#image-form-modal').modal('toggle');
        }
      })
    }
  })
}

var reloadOwlCarouselManager = function() {
  var checkExist = setInterval(function() {
    if ($('.owl-stage-outer').length) {
      clearInterval(checkExist);

      $(document).on('click', '.owl-item', function(){
        $img = $(this).find('img');

        $('#image-form-modal').modal('toggle');

        $('.modal-image').attr('src', $img.attr('src'));
        $('.delete-attachment-btn').attr('data-id', $img.data('id'));
        $('.delete-attachment-btn').attr('data-index', $img.data('index'));
      });
    }
  }, 100); // check every 100ms

  $('#image-form-modal').on('hidden.bs.modal', function () {
    $('.modal-image').attr('src', '');
  });
}

var reloadEditItemInventoryEvent = function() {
  $('.edit-item-inventory').off('click').on('click', function(e) {
    e.preventDefault();

    $row = $(this).closest('tr');

    $.ajax({
      url: '/items/'+ itemId +'/item_inventories/'+ $(this).data('id') +'.json',
      method: 'GET',
      dataType: 'json',
      success: function(data) {
        $('[name=\'warehouse[id]\']').val(data.warehouse.id);

        $('.warehouse_ids').trigger('change');

        $('.edit-item-inventory').off('stockSectionIdPopulated').on('stockSectionIdPopulated', function(e) {
          for(var key in fields) {
            fields[key].forEach(function(field) {
              var $input = $('[name=\''+ key +'['+ field +']\']');

              if(key === 'stock_section' && field === 'id')
                $input = $('.stock_section_ids');

              var val = data[key][field];

              $input.val(val);
            });
          }

          $('#item-inventory-by-serial-form-modal').modal('toggle');
        });
      }
    })
  })
}

var reloadDeleteItemInventoryEvent = function() {
  $('.destroy-item-inventory').off('click').on('click', function(e) {
    e.preventDefault();

    if(confirm('Are you sure?')) {
      $row = $(this).closest('tr');

      $.ajax({
        url: '/items/'+ itemId +'/item_inventories/'+ $(this).data('id') +'.json',
        method: 'DELETE',
        dataType: 'json',
        success: function(data) {
          $('#item-inventories-list').DataTable().row($row).remove().draw(false);

          ['on-stock', 'total'].forEach(function(sec) {
            $('.'+ sec).text(parseInt($('.'+ sec).text()) - 1);
          });
        }
      })
    }
  })
}

var reloadDeleteByBulkEvent = function() {
  $('.delete-by-bulk').off('click').on('click', function(e) {
    e.preventDefault();

    if(confirm('Are you sure?')) {
      var quantity;

      $('.quantity').each(function(e) {
        if($(this).val()) {
          quantity = $(this).val();
          return;
        }
      });

      $.ajax({
        url: '/items/'+ itemId +'/item_inventories/delete_by_bulk.json',
        method: 'DELETE',
        dataType: 'json',
        data: { by: 'bulk', quantity: quantity },
        success: function(data) {
          var count = 0;

          data.forEach(function(itemInventoryData) {
            $row = $('.destroy-item-inventory[data-id=\''+ itemInventoryData.id +'\']').closest('tr');

            $('#item-inventories-list').DataTable().row($row).remove().draw(true);

            count += 1;
          });

          ['on-stock', 'total'].forEach(function(sec) {
            var currentCount = parseInt($('.'+ sec).text()) - count;
            if(currentCount < 0) currentCount = 0;

            $('.'+ sec).text(currentCount);
          });

          $('#delete-by-bulk-form-modal').modal('hide');

          location.reload();
        }
      })
    }
  });
}

var getItemInventoryData = function(data) {
  var user = data.user.username;
  var stockSection = data.stock_section.name;

  var actionButtons = '';

  if(data.serial_id)
    actionButtons += '<a href=\'#\' class=\'edit-item-inventory\' data-id=\''+ data.id +'\'> \
      <i class=\'fa fa-edit\'></i> \
    </a>';

  actionButtons += '<a href=\'#\' class=\'destroy-item-inventory\' data-id=\''+ data.id +'\'> \
      <i class=\'fa fa-trash\'></i> \
    </a>';

  // var tmp = [
  //   user,
  //   stockSection,
  //   _.startCase(data.item_status),
  //   data.serial_id ? data.serial_id : 'N/A',
  //   actionButtons
  // ];

  var tmp = {
    username: user,
    name: stockSection,
    item_status: _.startCase(data.item_status),
    serial_id: data.serial_id ? data.serial_id : 'N/A',
    actions: actionButtons
  };

  return tmp;
}
