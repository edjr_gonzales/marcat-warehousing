var fields = {
  'stock_section': ['id', 'name', 'sc_code'],
  'warehouse': ['id']
}

var $row = null;
var warehouseId = $('[name=\'warehouse[id]\']').val();

$(function() {
  $('#personnel_ids').select2();

  if($('#stock-sections-list')) {
    $('#stock-sections-list').DataTable({
      'drawCallback': function(settings) {
        reloadEditStockSectionEvent();
        reloadDeleteStockSectionEvent();
      }
    });
  }

  if($('#save-stock-section-btn')) {
    $('#save-stock-section-btn').off('click').on('click', function(e) {
      e.preventDefault();

      var params = {};

      for(var key in fields) {
        fields[key].forEach(function(field) {
          var $input = $('[name=\''+ key +'['+ field +']\']');
          var value = $input.val();

          params[key + '['+ field +']'] = value;
        });
      }

      if($('[name=\'stock_section[id]\']').val()) {
        params['_method'] = 'put';
        var stockSectionId = $('[name=\'stock_section[id]\']').val();

        $.ajax({
          url: '/warehouses/'+ warehouseId +'/stock_sections/'+ stockSectionId +'.json',
          method: 'POST',
          dataType: 'json',
          data: params,
          success: function(data) {
            $('#stock-section-form-modal').modal('toggle');
            $('#stock-sections-list').DataTable().row($row).data(getStockSectionData(data)).draw(false);
          }
        });
      } else {
        $.ajax({
          url: '/warehouses/'+ warehouseId +'/stock_sections.json',
          method: 'POST',
          dataType: 'json',
          data: params,
          success: function(data) {
            $('#stock-section-form-modal').modal('toggle');
            $('#stock-sections-list').DataTable().row.add(getStockSectionData(data)).draw(false);
          }
        })
      }
    })
  }
})

var reloadEditStockSectionEvent = function() {
  $('.edit-stock-section').off('click').on('click', function(e) {
    e.preventDefault();

    $row = $(this).closest('tr');

    $.ajax({
      url: '/warehouses/'+ warehouseId +'/stock_sections/'+ $(this).data('id') +'.json',
      method: 'GET',
      dataType: 'json',
      success: function(data) {
        for(var key in fields) {
          var dataKey = key;

          if(key === 'warehouse')
            continue;

          fields[key].forEach(function(field) {
            var $input = $('[name=\''+ key +'['+ field +']\']');
            var val = data[dataKey][field];

            $input.val(val);
          })
        }

        $('#stock-section-form-modal').modal('toggle');
      }
    })
  })
}

var reloadDeleteStockSectionEvent = function() {
  $('.destroy-stock-section').off('click').on('click', function(e) {
    e.preventDefault();

    if(confirm('Are you sure?')) {
      $row = $(this).closest('tr');

      $.ajax({
        url: '/warehouses/'+ warehouseId +'/stock_sections/'+ $(this).data('id') +'.json',
        method: 'DELETE',
        dataType: 'json',
        success: function(data) {
          $('#stock-sections-list').DataTable().row($row).remove().draw(false);
        }
      })
    }
  })
}

var getStockSectionData = function(data) {
  var stockSection = data.stock_section;

  return [
    stockSection.name,
    stockSection.sc_code,
    '<a href=\'#\' class=\'edit-stock-section\' data-id=\''+ stockSection.id +'\'> \
      <i class=\'fa fa-edit\'></i> \
    </a> \
    <a href=\'#\' class=\'destroy-stock-section\' data-id=\''+ stockSection.id +'\'> \
      <i class=\'fa fa-trash\'></i> \
    </a>'
  ]
}
