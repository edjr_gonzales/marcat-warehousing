$(function(){
	$('#order-slips-list').dataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#order-slips-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'warehouse' },
      { data: 'customer' },
      { data: 'status' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [2]
    	}
    ]
	});
});
