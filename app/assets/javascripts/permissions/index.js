$(function(){
	$('#permissions-list').dataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#permissions-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'name' },
      { data: 'permission_code' },
      { data: 'description' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [3]
    	}
    ]
	});
});
