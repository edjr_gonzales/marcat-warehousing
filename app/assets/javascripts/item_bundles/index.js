$(function(){
  $('#item-bundles-list').dataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#item-bundles-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'name' },
      { data: 'bundle_set_quantity' },
      { data: 'items' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [3]
    	}
    ]
	});
});
