// RESOURCES AND FIELDS

var fields = {
  'address': ['id', 'basic_address', 'town_city', 'state_province', 'country'],
  'addressable': ['is_business_address', 'is_home_address', 'is_delivery_address', 'fax', 'delivery_schedule_day', 'business_name', 'tax_id', 'contact_info'],
  'resource': ['id', 'type']
};

var discountFields = {
  'discount': ['id', 'brand_id', 'discount', 'from_month', 'to_month'],
  'resource': ['id', 'type']
};

var contactInformationFields = {
  'contact_information': ['id', 'name', 'contact_number_1', 'contact_number_2'],
  'resource': ['id', 'type']
}

// GLOBAL $row

var $row = null;

// LOAD

window.GeneralFormHelper = window.GeneralFormHelper || {};
GeneralFormHelper.loadGeneralForm = function() {
  if(window.location.href.indexOf('#add-address') >= 0)
    $('#address-form-modal').modal('toggle');

  $('#addresses-list').DataTable({
    'drawCallback': function(settings) {
      reloadDestroyAddressEvent();
      reloadEditAddressEvent();
      reloadMakePrimaryEvent();
    }
  });

  if(window.location.href.indexOf('#add-discount') >= 0)
    $('#discount-form-modal').modal('toggle');

  $('#discounts-list').DataTable({
    'drawCallback': function(settings) {
      reloadEditDiscountEvent();
      reloadDestroyDiscountEvent();
    }
  });

  if(window.location.href.indexOf('#add-contact-information') >= 0)
    $('#contact-information-form-modal').modal('toggle');

  $('#contact-informations-list').DataTable({
    'drawCallback': function(settings) {
      reloadDestroyContactInformationEvent();
      reloadEditContactInformationEvent();
    }
  });

  dicountFields(1);
  $('#discounting_type').change(function(){
    dicountFields(this.value);
  });

  $('#addressable_is_business_address').off('change').on('change', function(e){
    setupFields('business-section', this.checked)
  });

  loadDeliveryScheduleEvent();

  weekPickersConf = {
    mousedownSel: false
  }

  weekPickersConf2 = {
    mousedownSel: false
  }

  /*COUNTER DAY*/
  weekPickersConf.onChange = function () {
    $('#collection_information_counter_day').val($(this).weekLine('getSelected'));
  }

  $field1 = $('#week-picker-counter_day').weekLine(weekPickersConf);

  if($('#collection_information_counter_day').val())
    $field1.weekLine('setSelected', $('#collection_information_counter_day').val());

  /*COLLECTION DAY*/
  weekPickersConf2.onChange = function () {
    $('#collection_information_collection_day').val($(this).weekLine('getSelected'));
  }

  $field2 = $('#week-picker-collection_day').weekLine(weekPickersConf2);

  if($('#collection_information_collection_day').val())
    $field2.weekLine('setSelected', $('#collection_information_collection_day').val());

  $('#save-address-btn').off('click').on('click', function(e) {
    e.preventDefault();

    var params = {};

    for(var key in fields) {
      fields[key].forEach(function(field){
        var $input = $('[name=\''+ key +'['+ field +']\']');
        var value = field.startsWith('is_') ? $input.is(':checked') : $input.val();

        params[key +'['+ field +']'] = value;
      });
    }

    if($('[name=\'address[id]\']').val()){
      params['_method'] = 'put'
      var addressId = $('[name=\'address[id]\']').val();

      $.ajax({
        url: '/addresses/'+ addressId +'.json',
        method: 'POST',
        dataType: 'json',
        data: params,
        success: function(data){
          $('#address-form-modal').modal('toggle');
          $('#addresses-list').DataTable().row($row).data(getAddrData(data, false)).draw(false);
        }
      });
    } else{
      $.ajax({
        url: '/addresses.json',
        method: 'POST',
        dataType: 'json',
        data: params,
        success: function(data){
          $('#addresses-list').DataTable().row.add(getAddrData(data, false)).draw(false);
        }
      });
    }
  });

  $('#save-discount-btn').off('click').on('click', function(e){
    e.preventDefault();
    var params = {};
    var discount_arr = [];

    for(i=0; i < parseInt($('#discounting_type').val()); i++)
      discount_arr.push($('#discount_discount_' + i).val());

    for(var key in discountFields) {
      discountFields[key].forEach(function(field){

        var $input = $('[name=\''+ key +'['+ field +']\']');
        var value = field.startsWith('is_') ? $input.is(':checked') : $input.val();

        if(field == 'discount'){
          params[key +'['+ field +']'] = discount_arr;
        } else {
          params[key +'['+ field +']'] = value;
        }
      });
    }

    if($('[name=\'discount[id]\']').val()){
      params['_method'] = 'put'
      var discountID = $('[name=\'discount[id]\']').val();

      $.ajax({
        url: '/discounts/'+ discountID +'.json',
        method: 'POST',
        dataType: 'JSON',
        data: params,
        success: function(data){
          $('#discount-form-modal').modal('toggle');
          $('#discounts-list').DataTable().row($row).data(getDiscountData(data)).draw(false);
        }
      });
    } else {
      $.ajax({
        url: '/discounts.json',
        method: 'POST',
        dataType: 'JSON',
        data: params,
        success: function(data){
          $('#discount-form-modal').modal('toggle');
          $('#discounts-list').DataTable().row.add(getDiscountData(data)).draw(false);
        }
      });
    }
  });

  $('#save-contact-information-btn').off('click').on('click', function(e){
    e.preventDefault();

    var params = {};

    for(var key in contactInformationFields) {
      contactInformationFields[key].forEach(function(field){
        var $input = $('[name=\''+ key +'['+ field +']\']');
        var value = field.startsWith('is_') ? $input.is(':checked') : $input.val();

        params[key +'['+ field +']'] = value;
      });
    }

    if($('[name=\'contact_information[id]\']').val()){
      params['_method'] = 'put'
      var contactInfoId = $('[name=\'contact_information[id]\']').val();

      $.ajax({
        url: '/contact_informations/'+ contactInfoId +'.json',
        method: 'POST',
        dataType: 'json',
        data: params,
        success: function(data){
          $('#contact-information-form-modal').modal('toggle');
          $('#contact-informations-list').DataTable().row($row).data(getContactInfoData(data)).draw(false);

          reloadContactInformationSelectTags(data.contact_information, false);
        }
      });
    } else{
      $.ajax({
        url: '/contact_informations.json',
        method: 'POST',
        dataType: 'json',
        data: params,
        success: function(data){
          $('#contact-information-form-modal').modal('toggle');
          $('#contact-informations-list').DataTable().row.add(getContactInfoData(data)).draw(false);

          reloadContactInformationSelectTags(data.contact_information, false);
        }
      });
    }
  });

  $('#address-form-modal').on('hidden.bs.modal', function () {
    resetFields(fields);
    $('.delivery-schedule-section').css('display', 'none');
    $('div#week-picker-delivery_schedule_day a').removeAttr('class');
  });

  $('#discount-form-modal').on('hidden.bs.modal', function () {
    $('.discount-fields').val('');
    $('.monthrange').val('');
    $('#discounting_type').val(1);
    $('#discount_brand_id').val('');
    dicountFields(1);
  });

  $('#discount_from_month').datetimepicker({
    format:'Y/m/d',
    onShow:function( ct ){
     this.setOptions({
      maxDate:$('#discount_to_month').val()?$('#discount_to_month').val():false
     })
    },
    timepicker:false
   });
   $('#discount_to_month').datetimepicker({
    format:'Y/m/d',
    onShow:function( ct ){
     this.setOptions({
      minDate:$('#discount_from_month').val()?$('#discount_from_month').val():false
     })
    },
    timepicker:false
   });

  $('#contact-information-form-modal').on('hidden.bs.modal', function () {
    resetFields(contactInformationFields);
    $('.business-section').css('display', 'none');
  });
}

// FUNCTIONS

var reloadContactInformationSelectTags = function(data, destroy) {
  ['collection_information_contact_info', 'addressable_contact_info', 'accounting_information_contact_info'].forEach(function(field) {
    var $option = $('#'+ field +' > option[value='+ data.id +']');

    if(destroy){
      if($option.length)
        $option.remove();
    } else {
      if($option.length)
        $option.text(data.name);
      else
        $('#'+ field).append('<option value=\''+ data.id +'\'>'+ data.name +'</option>')
    }
  })
}

var getAddrData = function(data, previous) {
  var resAddr = data.addressable;
  var addr = data.address;

  var is_primary = null;
  var make_primary_btn = null;

  var $make_primary_btn = '<div class=\'td0\'><a class=\'btn btn-sm btn-info make-primary-btn\' data-id=\''+ addr.id +'\' data-resource-id=\''+ resAddr.customer_id +'\' data-addressable-id=\''+ resAddr.id +'\' href=\'#\' style=\'display: none;\'>Make Primary</a></div>';

  if(previous) {
    is_primary = 'No';
    make_primary_btn = $make_primary_btn;
  } else {
    is_primary = resAddr.is_primary ? 'Yes' : 'No'
    make_primary_btn = resAddr.is_primary ? '' : $make_primary_btn;
  }

  return [
    make_primary_btn + is_primary,
    resAddr.is_business_address ? 'Yes' : 'No',
    resAddr.is_home_address ? 'Yes' : 'No',
    resAddr.is_delivery_address ? 'Yes' : 'No',
    resAddr.fax,
    addr.town_city +', '+ addr.state_province +', '+ addr.country,
    '<a href=\'#\' class=\'edit-address\' data-id=\''+ addr.id +'\'> \
      <i class=\'fa fa-edit\'></i> \
    </a> \
    <a href=\'#\' class=\'destroy-address\' data-id=\''+ addr.id +'\'> \
      <i class=\'fa fa-trash\'></i> \
    </a>'
  ]
}

var getContactInfoData = function(data) {
  return [
    data.contact_information.name,
    data.contact_information.contact_number_1,
    data.contact_information.contact_number_2,
    '<a href=\'#\' class=\'edit-contact-information\' data-id=\''+ data.contact_information.id +'\'> \
      <i class=\'fa fa-edit\'></i> \
    </a> \
    <a href=\'#\' class=\'destroy-contact-information\' data-confirm=\'Are you sure?\' data-id=\''+ data.contact_information.id +'\'> \
      <i class=\'fa fa-trash\'></i> \
    </a>'
  ]
}

var getDiscountData = function(data) {
  var discount = data.discount.join('%, Less ');
  var _br = JSON.parse(brands);
  var brand_name = "";

  for(i = 0; i < _br.length; i++){
    if(data.brand_id == _br[i].id)
      brand_name = _br[i].name;
  }

  return [
    brand_name,
    "Less " + discount + "%",
    '<a href=\'#\' class=\'edit-discount\' data-id=\''+ data.id +'\'> \
      <i class=\'fa fa-edit\'></i> \
    </a> \
    <a href=\'#\' class=\'destroy-discount\' data-confirm=\'Are you sure?\' data-id=\''+ data.id +'\'> \
      <i class=\'fa fa-trash\'></i> \
    </a>'
  ]
}

var resetFields = function(flds) {
  for(var key in flds){
    if(key === 'resource')
      continue;

    flds[key].forEach(function(field){
      var $input = $('[name=\''+ key +'['+ field +']\']');

      if(field.startsWith('is_'))
        $input.prop('checked', false);
      else
        $input.val('');
    });
  }
}

var reloadMakePrimaryEvent = function() {
  reloadMouseEnterLeaveInMakePrimaryBtn();

  $('.make-primary-btn').off('click').on('click', function(e) {
    e.preventDefault();

    $row = $(this).closest('tr');

    var addressId = $(this).data('id');
    var resourceId = $(this).data('resource-id');
    var resourceType = $(this).data('resource-type');

    $.ajax({
      url: '/addresses/'+ addressId +'/make_primary.json',
      method: 'POST',
      dataType: 'json',
      data: { 'resource[id]': resourceId, 'resource[type]': resourceType, '_method': 'PATCH' },
      success: function(data) {
        $('#addresses-list').DataTable().row($row).data(getAddrData(data, false)).draw(false);

        if(data.previous_primary && data.previous_primary.address){
          var $previousPrimary = $('[data-address-id='+ data.previous_primary.address.id +']');
          $('#addresses-list').DataTable().row($previousPrimary).data(getAddrData(data.previous_primary, true)).draw(false);
        }
      }
    });
  });
}

var reloadMouseEnterLeaveInMakePrimaryBtn = function() {
  $('#addresses-list tbody tr').on('mouseenter', function(e) {
    $(this).find('.make-primary-btn').css('display', 'block');
  });

  $('#addresses-list tbody tr').on('mouseleave', function(e) {
    $(this).find('.make-primary-btn').css('display', 'none');
  });
}

var reloadDestroyAddressEvent = function() {
  $('.destroy-address').off('click').on('click', function(e){
    e.preventDefault();

    if(confirm('Are you sure?')){
      $row = $(this).closest('tr');

      $.ajax({
        url: '/addresses/'+ $(this).data('id') +'.json',
        method: 'DELETE',
        dataType: 'json',
        success: function(data){
          $('#addresses-list').DataTable().row($row).remove().draw(false);
        }
      });
    }
  });
}

var reloadEditAddressEvent = function() {
  $('.edit-address').off('click').on('click', function(e){
    e.preventDefault();

    $row = $(this).closest('tr');

    $.ajax({
      url: '/addresses/'+ $(this).data('id') +'.json',
      method: 'GET',
      dataType: 'json',
      success: function(data){
        for(var key in fields){
          var dataKey = key;

          if(key === 'resource')
            continue;

          fields[key].forEach(function(field){
            var $input = $('[name=\''+ key +'['+ field +']\']');

            if(field.startsWith('is_')){
              $input.prop('checked', data[dataKey][field]);

              if(key === 'addressable')
                setupFields('delivery-schedule-section', data[dataKey][field]);
            } else if(field == 'delivery_schedule_day') {
              setWeekLineData(data[dataKey][field], 'delivery_schedule_day');
            } else {
              var val = data[dataKey][field];

              if(dataKey === 'addressable' && field === 'contact_info')
                val = val ? val.id : null;

              $input.val(val);
            }
          });
        }

        $('#address-form-modal').modal('toggle');
      }
    })
  });
}

var reloadDestroyContactInformationEvent = function() {
  $('.destroy-contact-information').off('click').on('click', function(e){
    e.preventDefault();

    if(confirm('Are you sure?')){
      $row = $(this).closest('tr');

      $.ajax({
        url: '/contact_informations/'+ $(this).data('id') +'.json',
        method: 'DELETE',
        dataType: 'json',
        success: function(data){
          $('#contact-informations-list').DataTable().row($row).remove().draw(false);

          reloadContactInformationSelectTags(data.contact_information, true);
        }
      });
    }
  });
}

var reloadEditContactInformationEvent = function() {
  $('.edit-contact-information').off('click').on('click', function(e){
    e.preventDefault();

    $row = $(this).closest('tr');

    $.ajax({
      url: '/contact_informations/'+ $(this).data('id') +'.json',
      method: 'GET',
      dataType: 'json',
      success: function(data){
        for(var key in contactInformationFields){
          if(key === 'resource')
            continue;

          contactInformationFields[key].forEach(function(field){
            var $input = $('[name=\''+ key +'['+ field +']\']');

            if(field.startsWith('is_'))
              $input.prop('checked', data[key][field]);
            else
              $input.val(data[key][field]);
          });
        }

        $('#contact-information-form-modal').modal('toggle');
      }
    })
  });
}

var reloadEditDiscountEvent = function() {
  $('.edit-discount').off('click').on('click', function(e){
    e.preventDefault();

    $row = $(this).closest('tr');

    $.ajax({
      url: '/discounts/'+ $(this).data('id') +'.json',
      method: 'GET',
      dataType: 'json',
      success: function(data){
        $('#discount_id').val(data.id);

        if(data.brand_id)
          $('#discount_brand_id').val(data.brand_id);

        $('#discounting_type').val(data.discount.length);
        $('#discount_from_month').val(data.from_month);
        $('#discount_to_month').val(data.to_month);

        dicountFields(data.discount.length);

        for(i = 0; i < data.discount.length; i++)
          $('#discount_discount_' + i).val(data.discount[i]);

        $('#discount-form-modal').modal('toggle');
      }
    })
  });
}

var reloadDestroyDiscountEvent = function() {
  $('.destroy-discount').off('click').on('click', function(e){
    e.preventDefault();

    if(confirm('Are you sure?')){
      $row = $(this).closest('tr');

      $.ajax({
        url: '/discounts/'+ $(this).data('id') +'.json',
        method: 'DELETE',
        dataType: 'json',
        success: function(data){
          $('#discounts-list').DataTable().row($row).remove().draw(false);
        }
      });
    }
  });
}

var loadDeliveryScheduleEvent = function() {
  $('#addressable_is_delivery_address').off('change').on('change', function(e){
    setupFields('delivery-schedule-section', this.checked)
  });

  weekPickersConf3 = { mousedownSel: false }

  weekPickersConf3.onChange = function() {
    $('#addressable_delivery_schedule_day').val($(this).weekLine('getSelected'));
  }

  $field1 = $('#week-picker-delivery_schedule_day').weekLine(weekPickersConf3);

  if($('#week-picker-counter_day').val())
    $field1.weekLine('setSelected', $('#collection_information_counter_day').val());
}

var setupFields = function(section, checked) {
  disp = checked ? 'block' : 'none'

  $('.'+ section).css('display', disp);
}

var dicountFields = function(data){
	var html = "";

	for(i = 0; i < data; i++){
    html += "<div class=\"col\">";
    html +=   "<div class=\"form-group\">";
    html +=     "<label>Discount "+ (i + 1) +"</label>";
    html +=     "<input type=\"text\" name=\"discount_"+ i +"\" id=\"discount_discount_"+ i +"\" class=\"form-control discount-fields\" placeholder=\"Discount %\">";
    html +=   "</div>";
    html += "</div>";
	}

	$('#discount-container').html(html);
}

var setWeekLineData = function(data, target_id) {
	$('#week-picker-' + target_id).empty();
	$field = $('#week-picker-' + target_id).weekLine({
		mousedownSel: false,
		onChange: function(e) {
			if(target_id == 'delivery_schedule_day')
				$('#addressable_delivery_schedule_day').val($(this).weekLine('getSelected'));
		}
	});

	if(data){
		$field.weekLine('setSelected', data);
		$('#addressable_delivery_schedule_day').val(data);
	}
}
