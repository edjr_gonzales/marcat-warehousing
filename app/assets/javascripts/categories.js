var fields = {
  'sub_category': ['id', 'name', 'short_code'],
  'category': ['id']
}

var $row = null;
var categoryId = $('[name=\'category[id]\']').val();

$(function() {
	if($('#sub-categories-list')) {
    $('#sub-categories-list').DataTable({
      'drawCallback': function(settings) {
        reloadEditSubCategoryEvent();
        reloadDeleteSubCategoryEvent();
      }
    });
  }

	if($('#save-sub-category-btn')) {
		$('#save-sub-category-btn').off('click').on('click', function(e) {
			e.preventDefault();

			var params = {};

			for(var key in fields) {
        fields[key].forEach(function(field) {
          var $input = $('[name=\''+ key +'['+ field +']\']');
          var value = $input.val();

          params[key + '['+ field +']'] = value;
        });
      }

      if($('[name=\'sub_category[id]\']').val()) {
      	params['_method'] = 'put';
        var subCategoryId = $('[name=\'sub_category[id]\']').val();

      	$.ajax({
      		url: '/categories/'+ subCategoryId +'.json',
      		method: 'POST',
      		dataType: 'json',
      		data: params,
      		success: function(data) {
      			$('#sub-category-form-modal').modal('toggle');
            $('#sub-categories-list').DataTable().row($row).data(getSubCategoryData(data)).draw(false);
      		}
      	});
      } else {
      	$.ajax({
					url: '/categories.json',
					method: 'POST',
					dataType: 'json',
					data: params,
					success: function(data) {
						$('#sub-category-form-modal').modal('toggle');
						$('#sub-categories-list').DataTable().row.add(getSubCategoryData(data)).draw(false);
					}
				});
      }
		});
	}

	$('#sub-category-form-modal').on('hidden.bs.modal', function (e) {
	  $('[name=\'sub_category[id]\']').val(null);
	  $('[name=\'sub_category[name]\']').val(null);
	  $('[name=\'sub_category[short_code]\']').val(null);
	})
});

var reloadEditSubCategoryEvent = function() {
  $('.edit-sub-category').off('click').on('click', function(e) {
    e.preventDefault();

    $row = $(this).closest('tr');

    $.ajax({
      url: '/categories/'+ $(this).data('id') +'.json',
      method: 'GET',
      dataType: 'json',
      success: function(data) {
        for(var key in fields) {
          var dataKey = key;

          if(key === 'category')
            continue;

          fields[key].forEach(function(field) {
            var $input = $('[name=\''+ key +'['+ field +']\']');
            var val = data[field];

            $input.val(val);
          })
        }

        $('#sub-category-form-modal').modal('toggle');
      }
    })
  })
}

var reloadDeleteSubCategoryEvent = function() {
  $('.destroy-sub-category').off('click').on('click', function(e) {
    e.preventDefault();

    if(confirm('Are you sure?')) {
      $row = $(this).closest('tr');

      $.ajax({
        url: '/categories/'+ $(this).data('id') +'.json',
        method: 'DELETE',
        dataType: 'json',
        success: function(data) {
          $('#sub-categories-list').DataTable().row($row).remove().draw(false);
        }
      })
    }
  })
}

var getSubCategoryData = function(data) {
  var subCategory = data;

  return [
    subCategory.name,
    subCategory.short_code,
    '<a href=\'#\' class=\'edit-sub-category\' data-id=\''+ subCategory.id +'\'> \
      <i class=\'fa fa-edit\'></i> \
    </a> \
    <a href=\'#\' class=\'destroy-sub-category\' data-id=\''+ subCategory.id +'\'> \
      <i class=\'fa fa-trash\'></i> \
    </a>'
  ]
}
