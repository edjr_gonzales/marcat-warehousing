$(function() {
	$('#return-slips-list').dataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#return-slips-list').data('source')
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'sku' },
      { data: 'item_inventory_serial' },
      { data: 'description' },
      { data: 'reasons' },
      { data: 'total_quantity' },
      { data: 'unit_price' },
      { data: 'price' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [7]
    	}
    ]
	});
})
