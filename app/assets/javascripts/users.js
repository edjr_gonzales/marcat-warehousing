$(document).ready(function() {
	/*Uncomment if you want a single selection of roles*/
	// $(".chkbox").click(function() {
	// 	$('.chkbox').not(this).prop('checked', false);
	// });

	if(typeof action !== 'undefined')
		if(action == 'new') $('#username').val('');

	var allowsubmit = false;
	//on keypress
	$('.match-password').keyup(function(e){
		//get values
		console.log("keyup");
		var pass = $('#password').val();
		var confpass = $('#confirm_password').val();

		//check the strings
		if(pass == confpass){
			//if both are same remove the error and allow to submit
			$('.error').text('');
			$('.match-password').css('border-color', 'green');

			allowsubmit = true;
		} else {
			//if not matching show error and not allow to submit
			$('.error').text('Password not matching');
			$('.match-password').css('border-color', 'red');
			allowsubmit = false;
		}
	});

	//jquery form submit
	$('#user-form').submit(function(){

		var pass = $('#password').val();
		var confpass = $('#confirm_password').val();

		//just to make sure once again during submit
		//if both are true then only allow submit
		if(pass == confpass){
			allowsubmit = true;
		}
		if(allowsubmit){
			return true;
		}else{
			return false;
		}
	});

	if ($('#roles_salesmen')) {
		$('#roles_salesmen').off('change').on('change', function (e) {
			if ($(this).prop('checked')) {
				$('#user-role-permissions-hr').css('display', 'none');
				$('#user-role-permissions-section').css('display', 'none');
			} else {
				$('#user-role-permissions-hr').css('display', 'block');
				$('#user-role-permissions-section').css('display', 'block');
			}
		});

		if ($('#roles_salesmen').prop('checked')) {
			$('#user-role-permissions-hr').css('display', 'none');
			$('#user-role-permissions-section').css('display', 'none');
		} else {
			$('#user-role-permissions-hr').css('display', 'block');
			$('#user-role-permissions-section').css('display', 'block');
		}
	}
});
