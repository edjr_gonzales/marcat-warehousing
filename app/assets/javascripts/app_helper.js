// HELPER FUNCTIONS

window.AppHelper = window.AppHelper || {};
AppHelper.formatCurrency = function(ctrl, persisted) {
  //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
  if(event)
    if (event.keyCode >= 37 && event.keyCode <= 40) return;

  var val = ctrl.value;

  val = val.replace(/,/g, "");
  ctrl.value = "";
  val += '';
  x = val.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';

  var rgx = /(\d+)(\d{3})/;

  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }

  ctrl.value = x1 + x2;
}

AppHelper.checkNumeric = function() {
  return event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 46;
}

AppHelper.loader = function(type, action) {
  switch(action) {
    case 'hide':
      AppHelper._hideLoader(type);

      break;
    case 'show':
      AppHelper._showLoader(type);

      break;
    case 'toggle':
      const isActive = $('.app-loader').css('display') === 'none' &&
        $('.app-overlay').css('display') === 'none';

      if(isActive)
        AppHelper._showLoader(type);
      else
        AppHelper._hideLoader(type);

      break;
  }
}

AppHelper._showLoader = function(type) {
  $('.'+ type +'-loader').css('display', 'block');
  $('.'+ type +'-overlay').css('display', 'block');
}

AppHelper._hideLoader = function(type) {
  $('.'+ type +'-loader').css('display', 'none');
  $('.'+ type +'-overlay').css('display', 'none');
}

AppHelper.parseMoney = function(val) {
  let floatVal = parseFloat(val.replace(/,/g, ''));
  return isNaN(floatVal) ? 0 : floatVal;
};

// ON LOADS

$(function() {
  // HIDE LOADER AFTER PAGE LOADS
  AppHelper.loader('page', 'hide');

  // TIME ONLY PICKERS LOADER
  $('.time-only-picker').datetimepicker({
    datepicker: false,
    format:'H:i',
    mask: true
  });

  // CURRENCY FIELDS LOADER
  $('.currency-field').on('keypress', function(e){
    return AppHelper.checkNumeric();
  }).on('keyup', function(e){
    AppHelper.formatCurrency(this);
  });

  // NUMERIC FIELDS LOADER
  $('.numeric-field').on('keypress', function(e){
    return AppHelper.checkNumeric();
  });

  // ALERT AUTO HIDE
  $('.app-alert').fadeTo(3000, 300).fadeOut(300);

  $('.pagesDropdown').off('click').on('click', function(e) {
    $('.pagesDropdown').each(function(e) {
      $(this).next('.dropdown-menu').slideUp(300);
    });

    if($(this).parent().hasClass('show')) {
      $(this).next('.dropdown-menu').slideUp(300);
    } else {
      $(this).next('.dropdown-menu').slideDown(300);
    }
  });

  $('.owl-carousel').owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    responsive:{
      0: {
        items:1
      },
      600: {
        items:3
      },
      1000: {
        items:5
      }
    }
  })
});
