$(function() {
  const table = $('#customers-list').DataTable({
    processing: true,
    serverSide: true,
    ajax: {
      url: $('#customers-list').data('source'),
      data: function(d) {
        d.customer_status = $('#listing').val();
      }
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'checkbox' },
      { data: 'name' },
      { data: 'current_balance' },
      { data: 'status' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
      {
        bSortable: false,
        aTargets: [0, 4]
      }
    ]
  });

  $('#listing').change(function(){
    table.ajax.reload();
  });

  $('#chkbx').removeAttr('class');
  $('#chkbx').css('padding', '0px 11px');

  $('#master_checkbox').change(function() {
    const ischecked= $(this).is(':checked');

    $('input:checkbox').prop('checked', ischecked);
  });

  $('.archive-selected').click(function(){
    const result = confirm('Are you sure?');

    if (result){
      $('.ids').each(function(){
        if($(this).is(':checked')){
          $.ajax({
            url: '/customers/'+ $(this).val() +'/archive',
            method: 'DELETE',
            dataType: 'json',
            success: function(data){
              table.ajax.reload();
            }
          });
        }
      });
    }
  });
});
