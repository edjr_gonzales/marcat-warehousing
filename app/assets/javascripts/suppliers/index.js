$(function() {
	const table = $('#suppliers-list').DataTable({
		processing: true,
    serverSide: true,
    ajax: {
      url: $('#suppliers-list').data('source'),
      data: function(d) {
        d.supplier_status = $('#listing').val();
      }
    },
    pagingType: 'full_numbers',
    columns: [
      { data: 'checkbox' },
      { data: 'name' },
      { data: 'email' },
      { data: 'tax_id' },
      { data: 'credit_term' },
      { data: 'current_balance' },
      { data: 'status' },
      { data: 'actions' }
    ],
    aoColumnDefs: [
    	{
    		bSortable: false,
    		aTargets: [0, 7]
    	}
    ]
	});

	$('#listing').change(function(){
		table.ajax.reload();
	});

	$('#chkbx').removeAttr('class');
	$('#chkbx').css('padding', '0px 11px');

	$('#master_checkbox').change(function() {
    const ischecked = $(this).is(':checked');

    $('input:checkbox').prop('checked', ischecked);
  });

  $('.archive-selected').click(function(){
		const result = confirm('Are you sure?');

		if(result){
			$('.ids').each(function(){
	  		if($(this).is(':checked')){
	  			$.ajax({
						url: '/suppliers/'+ $(this).val() +'/archive',
						method: 'DELETE',
						dataType: 'json',
						success: function(data){
							table.ajax.reload();
						}
					});
	  		}
	  	});
		}
	});
});
