# Notes:
# monetize needs rails-money gem
class ReceivableBreakdown < ApplicationRecord
	include UserAccountable

	belongs_to	:customer_address
	belongs_to 	:receivable

	monetize 	:net_sales_cents
	monetize 	:gross_sales_cents
	monetize	:net_amount_due_cents

	validates	:start_date, date: true
	validates	:end_date, date: { after_or_equal_to: :start_date }

	# serialize	:data, JSON

end

