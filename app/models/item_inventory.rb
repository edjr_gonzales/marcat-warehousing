class ItemInventory < ApplicationRecord
  include UserAccountable

  belongs_to :user,
    foreign_key: :added_by
  belongs_to :stock_section
  belongs_to :item

  has_many :sales_details
  has_many :return_details

  # belongs_to SalesDeliveryReceipt
  # belongs_to SalesInvoiceReceipt
  # belongs_to ReturnSlip

  scope :bulk, -> (item_id, user_id, quantity) { where(item_id: item_id, item_status: :stock, added_by: user_id).limit(quantity) }

  enum item_status: { stock: 0, sold: 1, return: 2 }

  def get_available
  end
end
