	# Notes:
# monetize needs rails-money gem
class Receivable < ApplicationRecord
	include UserAccountable

	belongs_to	:customer
	belongs_to	:customer_address

	has_many 	:receivable_breakdowns

	monetize 	:net_sales_cents
	monetize 	:gross_sales_cents
	monetize	:net_amount_due_cents

	validates 	:process_date, presence: true, date: { before: Proc.new { Time.now } }
	validates	:start_date, date: true
	validates	:end_date, date: { after_or_equal_to: :start_date }
	validates	:sys_ref_number, presence: true, uniqueness: true

	attr_accessor :destroyed

	after_destroy 		:mark_as_destroyed
	before_validation	:create_id

	accepts_nested_attributes_for :receivable_breakdowns, allow_destroy: true

	def mark_as_destroyed
	  self.destroyed = true
	end

	after_initialize do |receivable|
		receivable.doc_data = {} if receivable.doc_data.nil?
		receivable.customer = Customer.take if customer.nil? && Customer.any?

		create_id
	end

	private
	def create_id
		if self.sys_ref_number.blank?
			day_count = Receivable.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count
			self.sys_ref_number = "RCV-#{Time.now.to_i}-#{'%03d' % (day_count + 1).to_s(16)}"
		end
	end
end
