class AccountingInformation < ApplicationRecord
	include Contactable
  include UserAccountable

  belongs_to :supplier

  enum credit_term: {
    _30_days: 0,
    _60_days: 1,
    _90_days: 2,
    _120_days: 3,
    _120_days_up: 4
  }
end
