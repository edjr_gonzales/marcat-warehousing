class Permission < ApplicationRecord
  include UserAccountable

  has_and_belongs_to_many :roles,
    join_table: :role_permissions

  has_and_belongs_to_many :users,
    join_table: :permissions_users

  enum permission_type: {
    root_permission: 0
  }
end
