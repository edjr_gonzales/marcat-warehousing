class OrderedItem < ApplicationRecord
  include UserAccountable

	belongs_to :order_slip
	belongs_to :item
end
