class StockSection < ApplicationRecord
  include UserAccountable

  belongs_to :warehouse

  has_many :item_inventories,
    foreign_key: :item_loc

  validates :name, uniqueness: true
  validates :sc_code, uniqueness: true

  def get_items
  end
end
