class ActivityLog < ApplicationRecord
  belongs_to :user_accountable,
  	polymorphic: true,
  	required: false
  belongs_to :user

  def self.log(user, action, message)
  	self.create!(user: user, log_type: action, log_message: message)
  end
end
