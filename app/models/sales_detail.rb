class SalesDetail < ApplicationRecord

  belongs_to :item_inventory,
    required: true

  belongs_to :sold_to,
    class_name: 'Customer',
    foreign_key: :sold_to

  belongs_to :customer_order_item

  belongs_to :sold_by,
  	class_name: 'User',
  	foreign_key: :sold_by

  after_save :sold_item_inventory
  before_create :sold_timestamp
  before_destroy :stock_item_inventory

  private
  def sold_timestamp
    self.sold_at = Time.now
  end

  def sold_item_inventory
    self.item_inventory.update_attribute(:item_status, ItemInventory.item_statuses[:sold])
  end

  def stock_item_inventory
    self.item_inventory.update_attribute(:item_status, ItemInventory.item_statuses[:stock])
  end
end
