class CollectionInformation < ApplicationRecord
	include Contactable
  include UserAccountable

	attr_accessor :counter_time_from, :counter_time_to
	attr_accessor :collection_time_from, :collection_time_to

  belongs_to :customer

  enum credit_term: { _30_days: 0, _60_days: 1, _90_days: 4, _120_days: 2, _120_days_up: 3 }

  before_save :set_counter_and_collection_time

  def get_counter_time
  	return [] if self.counter_time.nil?

  	self.counter_time.split(' - ')
  end

  def get_collection_time
  	return [] if self.collection_time.nil?

  	self.collection_time.split(' - ')
  end

  private

  def set_counter_and_collection_time
  	self.counter_time = "#{self.counter_time_from} - #{self.counter_time_to}"
  	self.collection_time = "#{self.collection_time_from} - #{self.collection_time_to}"
  end
end
