module Contactable
  extend ActiveSupport::Concern

  included do
    attr_accessor :contact_info

    has_many :branch_assignments, as: :branchable
    has_many :contact_informations, through: :branch_assignments
  end

  def contact
    self.contact_informations.first
  end

  def set_contact(contact)
    self.contact_informations.delete_all
    self.contact_informations << contact
  end
end
