module UserAccountable
  extend ActiveSupport::Concern

  included do
    has_many :activity_logs,
      as: :user_accountable,
      class_name: 'ActivityLog'
  end

  def log(user, action, message)
    if self.destroyed?
      ActivityLog.log(user, action, message);
    else
      self.activity_logs.create!(user: user, log_type: action, log_message: message)
    end
  end
end
