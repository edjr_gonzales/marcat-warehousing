class User < ApplicationRecord
  include UserAccountable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :warehouse,
    required: false

	has_and_belongs_to_many :roles,
    join_table: :user_roles

  has_and_belongs_to_many :permissions,
    join_table: :permissions_users

  has_and_belongs_to_many :checked_customer_orders,
    class_name: 'CustomerOrder'

  has_many :user_accountable_logs,
    class_name: 'ActivityLog',
    as: :user_accountable
  has_one :contact_information,
    as: :contactable,
    dependent: :destroy
  has_many :updated_logs,
    class_name: 'ActivityLog',
    foreign_key: :updated_by
  has_many :added_inventory_items,
    class_name: 'ItemInventory',
    foreign_key: :added_by
  has_many :sold_inventory_items,
    class_name: 'ItemInventory',
    foreign_key: :sold_by
  has_many :customers,
    class_name: 'Customer',
    foreign_key: :salesman_id

  belongs_to :warehouse

  scope :saved, -> { where.not(status: :unsaved) }
  scope :salesmen, -> { joins([:roles]).where(roles: { role_code: 'salesmen' }) }

  enum status: {
    _new: 0,
    active: 1,
    inactive: 2,
    deleted: 3,
    unsaved: 4
  }

  def authenticate
  end

  def is_root_admin?
    self.has_permission?('root_allow_root')
  end

  def is_salesman?
    self.roles.where(roles: { role_code: 'salesman' })
  end

  def has_role?(code)
    self.roles.where(role_code: code).present?
  end

  def has_permission?(code)
    self.permissions.where(permission_code: code).present?
  end

  def reset_password
  end

  def set_password(pwd)
  end
end
