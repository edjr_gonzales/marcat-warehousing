class CustomerAddress < ApplicationRecord
  include Contactable

	belongs_to :customer, required: false

  has_many :addresses,
  	as: :addressable,
  	dependent: :destroy

  scope :billing, -> { where(is_business_address: true) }
  scope :primary, -> { where(is_primary: true) }
  scope :home, -> { where(is_home_address: true) }
end
