class BundledItem < ApplicationRecord
  include UserAccountable

  belongs_to :item_bundle
  belongs_to :item
end
