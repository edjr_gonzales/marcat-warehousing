class Customer < ApplicationRecord
  include UserAccountable

  has_many :customer_addresses,
    dependent: :destroy
  has_many :addresses,
    as: :addressable,
    through: :customer_addresses
  has_many :discounts,
    as: :discountable,
    dependent: :destroy
  has_many :customer_support
  has_many :contact_informations,
    as: :contactable,
    dependent: :destroy
  has_many :bought_inventory_items,
    class_name: 'SalesDetail',
    foreign_key: :sold_to
  has_many :order_slips,
    as: :orderable

  has_one :collection_information

  belongs_to :user,
    optional: true,
    foreign_key: :salesman_id

  enum classification: {
    outright: 0,
    concessionaire: 1
  }
  enum payment_term: {
    cash_on_delivery: 0,
    _30_days: 1,
    _60_days: 2,
    _120_days: 3,
    _120_days_up: 4
  }
  enum type_of_receipt: {
    delivery_receipt: 0,
    sales_invoice: 1,
    order_sheet: 2
  }
  enum status: {
    _new: 0,
    active: 1,
    inactive: 2,
    archived: 3,
    unsaved: 4
  }

  validates :name, presence: true
  # validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }

  scope :saved, -> { where.not(status: :unsaved) }

  def recalculate_balance
  end

  def apply_discount(amount)
  end

  def last_invoice
  end
end
