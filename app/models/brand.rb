class Brand < ApplicationRecord
  include UserAccountable

	has_many :discounts
  has_many :items

  validates :name, uniqueness: true
  validates :short_code, uniqueness: true
end

