class ContactInformation < ApplicationRecord
  include UserAccountable

	belongs_to :contactable, polymorphic: true
	has_many :branch_assignments

	before_destroy :delete_assoc

	private

	def delete_assoc
		self.branch_assignments.destroy_all
	end
end
