class ReturnSlip < ApplicationRecord
  include UserAccountable

  serialize :reasons, Array

  has_many :return_details
  belongs_to :item
end
