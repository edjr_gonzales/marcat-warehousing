class Role < ApplicationRecord
  include UserAccountable

  has_and_belongs_to_many :users,
    join_table: :user_roles
  has_and_belongs_to_many :permissions,
    join_table: :role_permissions

  def has_permission?(code)
    self.permissions.where(permission_code: code).present?
  end

  def has_role?(code)
    Role.where(role_code: code).present?
  end
end
