# Notes:
# monetize needs rails-money gem
class CustomerOrderItem < ApplicationRecord
	include UserAccountable

	belongs_to	:customer_order
	belongs_to 	:item
	has_many		:sales_details, dependent: :destroy

	monetize 	:gross_amount_cents
	monetize	:net_amount_cents

	before_save :calculate_amount

	after_initialize do |order|

		if order.sales_data.nil? || order.sales_data.empty?
			order.sales_data = {
				discounts: {
					brands: []
				}
			}
		end
	end

	def calculate_amount
		self.gross_amount = self.net_amount = sales_details.size * item.price

		# apply brand discounts
		self.sales_data['discounts']['brands'] = []
		customer_order.customer.discounts.each do |dc|
			if dc.brand.present? && dc.brand == item.brand
				if !dc.bound_by_date || (dc.bound_by_date && dc.within_promo_date(customer_order.sale_date))
					dc.discount.each do |pct|
						self.sales_data['discounts']['brands'] << dc.to_json(include: :brand)
						self.net_amount -= (self.net_amount * (pct.to_f / 100))
					end
				end
			end
		end

		self.gross_amount
	end
end
