# Notes:
# monetize needs rails-money gem
class CustomerOrderItem < ApplicationRecord
	include UserAccountable

	belongs_to	:customer_order

	validates :order_status, presence: true
	validates :status_detail, presence: true
end
