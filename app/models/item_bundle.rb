class ItemBundle < ApplicationRecord
  include UserAccountable

  has_many :bundled_items

  has_many :items,
    through: :bundled_items,
    foreign_key: :item_bundle_id
end
