class Address < ApplicationRecord
  include UserAccountable

	belongs_to :addressable, polymorphic: true
end
