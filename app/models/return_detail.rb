class ReturnDetail < ApplicationRecord
  belongs_to :item_inventory
  belongs_to :return_slip

  enum return_status: { success: 0 }
end
