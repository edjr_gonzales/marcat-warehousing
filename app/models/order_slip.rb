class OrderSlip < ApplicationRecord
	include UserAccountable

	has_many :ordered_items

	has_many :items,
		through: :ordered_items,
		foreign_key: :order_slip_id
	belongs_to :warehouse
	belongs_to :orderable, polymorphic: true

	enum status: {
		_new: 0,
    active: 1,
    inactive: 2,
    archived: 3,
    unsaved: 4
	}
end
