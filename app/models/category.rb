class Category < ApplicationRecord
  include UserAccountable

  # has_and_belongs_to_many :items,
  #   join_table: 'categories_items',
  #   foreign_key: :category_id
  has_many :items

  has_many :sub_categories,
    class_name: 'Category',
    foreign_key: :parent_id

  belongs_to :parent_category,
    class_name: 'Category',
    foreign_key: :parent_id,
    required: false

  enum status: {
    _new: 0,
    active: 1,
    inactive: 2,
    archived: 3,
    unsaved: 4
  }

  validates :name, uniqueness: true
  validates :short_code, uniqueness: true

  scope :saved, -> { where.not(status: :unsaved) }
  scope :main, -> { where(parent_id: nil) }

  def is_main?
    self.parent_id.nil?
  end

  def is_sub?
    !self.is_main?
  end
end
