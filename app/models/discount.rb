class Discount < ApplicationRecord
  include UserAccountable

	serialize :discount, Array

	belongs_to :discountable, polymorphic: true
	belongs_to :brand, optional: true


  def bound_by_date
    from_month.present? && to_month.present?
  end

  def within_promo_date(given_date)
    current = Date.new

    if from_month.present? && to_month.present?
      from_date = Date.parse(from_month).change(year: current.year)
      to_date = Date.parse(to_month).change(year: current.year)

      if from_date < to_date
        return given_date >= from_date && given_date <= to_date
      end
    end

    false
  end
end
