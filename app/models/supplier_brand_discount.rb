class SupplierBrandDiscount < ApplicationRecord
  belongs_to :supplier
  belongs_to :brand
end
