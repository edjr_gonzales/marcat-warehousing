class Supplier < ApplicationRecord
  include UserAccountable

  has_many :supplier_addresses,
    dependent: :destroy
  has_many :addresses,
    as: :addressable,
    through: :supplier_addresses
  has_many :discounts,
    as: :discountable,
    dependent: :destroy
  has_many :contact_informations,
    as: :contactable,
    dependent: :destroy

  has_one :accounting_information

  enum payment_term: {
    weekly: 0,
    monthly: 1,
    semi_annual: 2,
    annual: 3
  }
  enum status: {
    _new: 0,
    active: 1,
    inactive: 2,
    archived: 3,
    unsaved: 4
  }

  scope :saved, -> { where.not(status: :unsaved) }

  def recalculate_balance
  end

  def apply_discount(discount)
  end

  def last_invoice
  end
end
