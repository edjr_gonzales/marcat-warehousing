class ItemImage < ApplicationRecord
  include UserAccountable

  belongs_to :item

  has_one_attached :image
end
