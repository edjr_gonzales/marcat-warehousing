class Group < ApplicationRecord
  include UserAccountable

  serialize :rights, JSON

  has_and_belongs_to_many :users,
    join_table: :users_groups

  def has_member?(user)
    joins(:users).where(users: { id: user.id }).present?
  end
end
