class Item < ApplicationRecord
  include UserAccountable

  has_many_attached :images

  belongs_to :brand,
    required: false

  belongs_to :category,
    required: false

  has_many :bundled_items, dependent: :destroy

  has_many :item_bundles,
    through: :bundled_items,
    foreign_key: :item_id

  has_many :order_slips,
    through: :order_items,
    foreign_key: :item_id

  has_and_belongs_to_many :sub_categories,
    class_name: 'Category',
    join_table: 'sub_categories_items',
    foreign_key: :item_id

  has_many :item_inventories, dependent: :destroy
  has_many :item_images, dependent: :destroy
  has_many :return_slips, dependent: :destroy

  validates :sku, uniqueness: true

  enum status: {
    _new: 0,
    active: 1,
    archived: 2,
    unsaved: 3
  }

  scope :saved, -> { where.not(status: :unsaved) }

  def get_available
  end

  def generate_sku
    # 16
  end

  def saved?
    !self.unsaved?
  end

  def set_category(id)
    self.category = Category.find_by(id: id)
  end

  def set_sub_categories(ids)
    self.sub_categories.destroy_all

    return if ids.nil?

    ids.each{ |id| self.sub_categories << Category.find(id) }
  end
end
