class BranchAssignment < ApplicationRecord
	belongs_to :branchable,
		polymorphic: true
	belongs_to :contact_information
end
