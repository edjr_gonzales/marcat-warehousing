class SupplierAddress < ApplicationRecord
  include Contactable
  include UserAccountable

	belongs_to :supplier, required: false

  has_many :addresses,
  	as: :addressable,
  	dependent: :destroy
end
