	# Notes:
# monetize needs rails-money gem
class CustomerOrder < ApplicationRecord
	include UserAccountable

	# default_scope { order(sale_date: :desc) }

	belongs_to	:customer
	belongs_to	:warehouse
	belongs_to	:sales_person, class_name: 'User', foreign_key: 'salesman_id', optional: true
	belongs_to	:billing_address, class_name: 'CustomerAddress', foreign_key: 'billing_address_id'
	belongs_to	:shipping_address, class_name: 'CustomerAddress', foreign_key: 'shipping_address_id'

	has_many 	:customer_order_items
	has_and_belongs_to_many :customer_order_checkers, class_name: 'User'
	has_many	:customer_order_statuses, -> { order 'created_at desc' }

	monetize 	:net_sales_cents
	monetize 	:gross_sales_cents
	monetize	:net_amount_due_cents

	validates	:trans_ref_number, presence: true, uniqueness: true
	validates :billing_address, :shipping_address, :customer, :warehouse, presence: true, if: :is_saved?

	attr_accessor :destroyed

	after_destroy :mark_as_destroyed

	before_validation	:create_id, :calculate_amount, :fill_missing_fields

	accepts_nested_attributes_for :customer_order_items, allow_destroy: true

	validate :shipping_address_is_delivery_address, if: :is_saved?
	validate :delivery_address_must_have_tax, if: :is_saved?

	def mark_as_destroyed
	  self.destroyed = true
	end

	after_initialize do |order|
		order.doc_data = {
			discounts: []
			} if order.doc_data.nil?
		create_id
	end

	def checker_ids
		self.customer_order_checkers.collect{ |chkr| chkr.id }
	end

	def checker_ids=(ids)
		self.customer_order_checkers = User.find(ids.reject { |id| id.empty? })
	end

	def is_saved?
		self.saved
	end

	private
	def fill_missing_fields
		@sale_date = Time.now if @sale_date.blank?
	end

	def calculate_amount
		gross = 0
		net = 0
		customer_order_items.each do |oi|
			gross += oi.calculate_amount
			net += oi.net_amount
		end

		self.gross_sales = gross
    self.net_sales = net

		# apply non-brand discounts
    self.doc_data["discounts"] = []
		customer.discounts.each do |dc|
			if dc.brand.nil?
				if !dc.bound_by_date || (dc.bound_by_date && dc.within_promo_date(customer_order.sale_date))
					dc.discount.each do |pct|
						self.doc_data["discounts"] << pct.to_f
						net -= (net * (pct.to_f / 100))
					end
				end
			end
		end

		self.net_amount_due = net

		self.gross_sales
	end

	def create_id
		if self.trans_ref_number.blank?
			day_count = CustomerOrder.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count
			self.trans_ref_number = "CO-#{Time.now.to_i}-#{'%03d' % (day_count + 1).to_s(16)}"
		end
	end

	def shipping_address_is_delivery_address
		unless shipping_address.is_delivery_address
			errors.add(:base, 'shipping address is not a valid delivery address')
		end
	end

	def delivery_address_must_have_tax
		unless billing_address.tax_id.present?
			errors.add(:base, 'billing address does not have valid tax information')
		end
	end
end
