class Warehouse < ApplicationRecord
  include UserAccountable

  has_many :managers,
    class_name: 'User'
  has_many :personnels,
    class_name: 'User'
  has_many :stock_sections

  validates :name, uniqueness: true
  validates :wh_code, uniqueness: true

  enum status: {
    _new: 0,
    active: 1,
    archived: 2,
    unsaved: 3
  }

  scope :saved, -> { where.not(status: :unsaved) }

  def get_items
  end
end
