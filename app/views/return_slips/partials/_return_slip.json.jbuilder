json.extract! return_slip, :id, :created_at, :updated_at
json.url return_slip_url(return_slip, format: :json)
