json.address do
	json.extract! address, :id, :basic_address, :town_city, :state_province, :country, :created_at, :updated_at
end

json.addressable do
	json.extract! address.addressable, :is_primary, :is_business_address, :is_home_address, :is_delivery_address, :fax, :delivery_schedule_day, :business_name, :tax_id, :created_at, :updated_at
	json.contact_info address.addressable.contact
end
