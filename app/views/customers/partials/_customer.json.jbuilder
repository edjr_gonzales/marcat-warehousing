json.extract! customer, :id, :created_at, :updated_at
json.url permission_url(customer, format: :json)
json.addresses customer.customer_addresses do |address|
	json.extract! address, :id, :business_name, :is_primary, :is_business_address, :is_home_address, :is_delivery_address, :tax_id
	json.location address.addresses.first, :apt_bldg_suite, :unit, :floor, :lot_no, :street, :brgy, :district, :town_city, :postal_code, :state_province, :country if address.addresses.any?
end


