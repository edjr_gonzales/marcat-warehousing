json.extract! discount, :id, :brand_id, :discount, :from_month, :to_month, :created_at, :updated_at
json.url discount_url(discount, format: :json)
