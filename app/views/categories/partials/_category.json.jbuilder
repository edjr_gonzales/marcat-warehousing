json.extract! category, :id, :name, :short_code, :created_at, :updated_at
json.url category_url(category, format: :json)
