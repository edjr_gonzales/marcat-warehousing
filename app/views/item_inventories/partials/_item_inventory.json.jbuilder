json.item_inventory do
  json.extract! item_inventory, :id, :serial_id, :added_by, :item_status, :created_at, :updated_at
end

json.item do
  json.extract! item_inventory.item, :id, :brand_id, :name, :details, :sku, :price, :dealer_price, :srp_price, :created_at, :updated_at
end

json.stock_section do
  json.extract! item_inventory.stock_section, :id, :warehouse_id, :name, :sc_code, :created_at, :updated_at
end

json.warehouse do
  json.extract! item_inventory.stock_section.warehouse, :id, :name, :wh_code, :wh_address, :created_at, :updated_at
end
