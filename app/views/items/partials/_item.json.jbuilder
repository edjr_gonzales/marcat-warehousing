json.extract! item, :id, :brand, :name, :details, :is_bundle, :sku, :price, :dealer_price, :srp_price, :created_at, :updated_at
json.url item_url(item, format: :json)
