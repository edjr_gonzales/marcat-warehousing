json.extract! brand, :id, :name, :short_code, :created_at, :updated_at
json.url brand_url(brand, format: :json)
