json.contact_information do
	json.extract! contact_information, :id, :name, :contact_number_1, :contact_number_2, :created_at, :updated_at
end
