json.extract! order_slip, :id, :created_at, :updated_at
json.url order_slip_url(order_slip, format: :json)
