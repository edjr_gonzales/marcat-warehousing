class ItemToOrderHandler
  include ActiveModel::Validations
  include ActiveModel::Callbacks
  include ActiveModel::Serializers::JSON

  validates :item_inventory, presence: true
  validates :customer_order, presence: true

  attr_accessor :item_inventory, :customer_order, :order_item, :sales_detail, :op
  attr_reader :result

  define_model_callbacks :initialize, only: [:after]
  after_initialize :valid?

  def attributes
    { 'result' => { status: :ok } }
  end

  def self.execute(item_id, serial_id, customer_order_id, op)
    self.new(item_id, serial_id, customer_order_id, op).tap(&:perform_op)
  end

  def initialize(item_id, serial_id, customer_order_id, op)
    @op = op
    @item = Item.find(item_id)

    @item_inventory = serial_id.present? && @item.is_identifiable ? @item.item_inventories.where(item_status: ItemInventory.item_statuses[:stock], serial_id: serial_id).take : @item.item_inventories.where(item_status: ItemInventory.item_statuses[:stock]).first

    @customer_order = CustomerOrder.find(customer_order_id)

    # seek inventory if deduct op
    if @op == 'sub'
      coi = @customer_order.customer_order_items.where(item: @item).take

      unless coi.nil?
        sd = coi.sales_details.joins(:item_inventory).first

        if serial_id.present? && @item.is_identifiable
          sd = coi.sales_details.joins(:item_inventory).where("item_inventory.serial_id = ?", serial_id).take
        end

        @item_inventory = sd.item_inventory unless sd.nil?
      end
    end

    if @customer_order.customer.user.nil?
      @customer_order.update_attribute(:sales_person, User.salesmen.saved.take) if @customer_order.sales_person.nil?
    else
      @customer_order.update_attribute(:sales_person, @customer_order.customer.user) if @customer_order.sales_person.nil?
    end

  end

  def result
    if valid? && order_item.valid? &&
      ((sales_detail.present? && sales_detail.valid?) || sales_detail.nil?)

      @result = {
        status: :created
      }
    else
      errors_msgs = []

      errors.full_messages.each do |msg|
        errors_msgs << msg
      end

      order_item.errors.full_messages.each do |msg|
        errors_msgs << "Order Details: #{msg}"
      end

      if sales_detail.present?
        sales_detail.errors.full_messages.each do |msg|
          errors_msgs << "Sales Details: #{msg}"
        end
      end

      @result = {
        status: :not_acceptable,
        errors: errors_msgs
      }
    end
  end

  def perform_op
    op == 'sub' ? perform_subtraction : ( op == 'del' ? perform_removal : perform_addition )
  end

  def perform_removal
    # retrieve item order record
    @order_item = customer_order.customer_order_items.joins(:item).where(item_id: @item.id).take

    ActiveRecord::Base.transaction do
      # if order contains the item
      unless @order_item.nil?
        @order_item.destroy
        customer_order.save
      end
    end #transaction
  end #perform_removal

  def perform_subtraction
    # retrieve item order record
    @order_item = customer_order.customer_order_items.joins(:item).where(item_id: @item.id).take

    ActiveRecord::Base.transaction do
      # if order contains the item
      unless @order_item.nil?
        # find sales details and destroy
        @order_item.sales_details.where(item_inventory_id: item_inventory.id).destroy_all if item_inventory.present?
        @order_item.destroy if @order_item.sales_details.empty?
        customer_order.save
      end
    end #transaction
  end #perform_subtraction

  def perform_addition
    # retrieve item order record
    @order_item = customer_order.customer_order_items.joins(:item).where(item_id: @item.id).take


    ActiveRecord::Base.transaction do
      if @order_item.nil?
        # create new item order record
        @order_item = customer_order.customer_order_items.create(item: item_inventory.item)
      end

      # create sales detail
      @sales_detail = order_item.sales_details.create(item_inventory: item_inventory, sold_to: customer_order.customer, sold_by: customer_order.sales_person)

      if @sales_detail.persisted?
        order_item.save
        customer_order.save
      end
    end #transaction
  end #perform_addition

end
