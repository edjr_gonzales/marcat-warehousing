class ItemSearchHandler
  include ActiveModel::Validations
  include ActiveModel::Callbacks
  include ActiveModel::Serializers::JSON

  attr_accessor :results, :keyword, :item_status

  def attributes
    { 'results' => [] }
  end

  def self.execute(search_str = '', item_status = ItemInventory.item_statuses[:stock])
    self.new(search_str, item_status).tap(&:perform_search)
  end

  def initialize(search_str = '', item_status = ItemInventory.item_statuses[:stock])
    @keyword = search_str
    @item_status = item_status
    @results = []
  end

  def search_non_identifiables
    if keyword.blank?
      Item.where(is_identifiable: false)
        .left_outer_joins(:item_inventories)
        .distinct
        .select("items.*, COUNT(item_inventories.id) AS stock_quantity, '' AS serial_no")
        .where("item_inventories.item_status" => item_status)
        .group("items.id").limit(25)
    else
      Item.where(is_identifiable: false)
        .where("items.name LIKE ?", "%#{keyword}%").or(Item.where(is_identifiable: false).where("items.sku LIKE ?", "%#{keyword}%"))
        .left_outer_joins(:item_inventories)
        .distinct
        .select("items.*, COUNT(item_inventories.id) AS stock_quantity, '' AS serial_no")
        .where("item_inventories.item_status" => item_status)
        .group("items.id").limit(25)
    end
  end

  def search_identifiables
    if keyword.blank?
      Item.where(is_identifiable: true)
        .left_outer_joins(:item_inventories)
        .distinct
        .select("items.*, 1 AS stock_quantity, item_inventories.serial_id AS serial_no")
        .where("item_inventories.item_status" => item_status)
        .limit(25)
    else
      Item.where(is_identifiable: true)
        .left_outer_joins(:item_inventories)
        .distinct
        .select("items.*, 1 AS stock_quantity, item_inventories.serial_id AS serial_no")
        .where("item_inventories.item_status" => item_status)
        .where("items.name LIKE ?", "%#{keyword}%").or(
          Item.where(is_identifiable: true)
            .left_outer_joins(:item_inventories)
            .distinct
            .select("items.*, 1 AS stock_quantity, item_inventories.serial_id AS serial_no")
            .where("item_inventories.item_status" => item_status).where("items.sku LIKE ?", "%#{keyword}%")
        ).or(
          Item.where(is_identifiable: true)
            .left_outer_joins(:item_inventories)
            .distinct
            .select("items.*, 1 AS stock_quantity, item_inventories.serial_id AS serial_no")
            .where("item_inventories.item_status" => item_status).where("item_inventories.serial_id LIKE ?", "%#{keyword}%")
        )
        .limit(25)
    end
  end

  def perform_search
    @results = search_non_identifiables + search_identifiables
  end
end
