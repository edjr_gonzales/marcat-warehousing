module ReceivableTemplates
	class Default
		def	self.doc_data_keys
			[ :tax_type, 
				:total => [ :margin_w_vat, :reduced_margin_w_vat, :vip_discount, :net_margin, :ewt, :total_deductions,  ],
				:invoice => [ :gross_sales, :margin_w_vat, :net_sales, :sales, :vat, :total_sales,
					:deductibles => [ :name, :value ] ]
			]
		end

		def self.breakdown_data_keys
			[ :dept_class, :contract_no, :margin_w_vat, :margin_w_vat_pct, :reduced_margin_w_vat, :vip_discount, :net_margin, :ewt, :total_deductions ]
		end
	end
end