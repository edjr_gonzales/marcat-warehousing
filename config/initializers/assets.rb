# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

defaults = ['index', 'new', 'create', 'edit', 'update', 'show']

precompiled_sets = {
	'app_helper': [],
	'general_form_helper': [],
	'block': [],
	'dashboards': ['index'],
	'users': ['index', 'new', 'create', 'edit', 'update', 'show', 'salesmen'],
	'roles': defaults,
	'permissions': defaults,
	'customers': defaults,
	'suppliers': defaults,
	'brands': defaults,
	'items': defaults,
	'item_bundles': defaults,
	'salesmen': defaults,
	'warehouses': defaults,
	'categories': defaults,
	'return_slips': defaults,
	'order_slips': defaults,
	'receivables': defaults,
	'customer_orders': defaults
}

precompiled_sets.each do |controller, actions|
	`touch #{Rails.root.join('app', 'assets', 'javascripts', "#{controller}.js")} || exit`
	`touch #{Rails.root.join('app', 'assets', 'stylesheets', "#{controller}.scss")} || exit`

	Rails.application.config.assets.precompile += [ "#{controller}.js", "#{controller}.css" ]

	actions.each do |action|
		`mkdir -p #{Rails.root.join('app', 'assets', 'javascripts', "#{controller}")}`
		`touch #{Rails.root.join('app', 'assets', 'javascripts', "#{controller}", "#{action}.js")} || exit`

		`mkdir -p #{Rails.root.join('app', 'assets', 'stylesheets', "#{controller}")}`
		`touch #{Rails.root.join('app', 'assets', 'stylesheets', "#{controller}", "#{action}.scss")} || exit`

		Rails.application.config.assets.precompile += [ "#{controller}/#{action}.js", "#{controller}/#{action}.css" ]
	end
end
