Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_scope :user do
    resources :users do
      collection do
        get 'salesmen'
      end
    end
  end

  root to: 'dashboards#index'

  resources :dashboards
  resources :permissions
  resources :brands
  resources :item_bundles
  resources :discounts
  resources :contact_informations
  resources :return_slips
  resources :order_slips

  resources :attachments, only: [:destroy]

  resources :categories do
    get 'sub_categories'
  end

  resources :items do
    resources :item_inventories do
      collection do
        delete 'delete_by_bulk'
        get 'serials'
        get 'on_stock'
      end
    end

    collection do
      match 'search-available', to: "items#search_available", via: [:get, :post]
      match 'search-return', to: "items#search_return", via: [:get, :post]
      match 'search-sold', to: "items#search_sold", via: [:get, :post]
    end
  end

  resources :warehouses do
    resources :stock_sections
  end

  resources :roles do
    get 'permissions'
    get 'update_permissions'
  end

  resources :addresses do
    member do
      patch 'make_primary'
    end
  end

  resources :customers do
    resources :customer_orders do
      member do
        match 'add-item-to-order', to: "customer_orders#add_item_to", via: [:get, :post]
        match 'deduct-item-from-order', to: "customer_orders#sub_item_from", via: [:delete]
        match 'remove-item-from-order', to: "customer_orders#del_item_from", via: [:delete]
        match 'items-of', to: "customer_orders#items_of", via: [:get, :post]
      end
    end

    member do
      delete 'archive'
    end
  end

  resources :suppliers do
    member do
      get 'delete_selected'
      delete 'archive'
    end
  end

  resources :receivables do
    collection do
      get :add_deductible_entry
      get :add_breakdown
      post :del_breakdown
      post :tpl_switcher
    end
  end
end
