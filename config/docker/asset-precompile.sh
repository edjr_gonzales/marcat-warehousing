#!/bin/sh

if [ "$RAILS_ENV" = "production" ]; then
	# Precompile assets for production
	bundle exec rake assets:precompile

	echo "Assets Pre-compiled!"
fi
