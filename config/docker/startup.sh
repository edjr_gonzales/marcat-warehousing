#!/bin/sh

# Wait for DB services
sh ./config/docker/wait-for-services.sh

# Prepare DB (Migrate - If not? Create db & Migrate)
sh ./config/docker/prepare-db.sh

sh ./config/docker/install-gems.sh

# Pre-comple app assets
sh ./config/docker/asset-precompile.sh

# Start Application
rm -fr ./tmp/pids/server.pid
bundle exec rails s -b 0.0.0.0
