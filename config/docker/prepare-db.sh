#!/bin/sh

# If the database exists, migrate. Otherwise setup (create and migrate)
(bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:create db:migrate) && bundle exec rake db:seed && bundle exec rake db:test:prepare
echo "Migration Done!"
